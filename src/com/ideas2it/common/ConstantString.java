package com.ideas2it.common;

/**
 * provides static access to all constant string 
 */
public class ConstantString {
    public static final String EMPLOYEE_OPTIONS = "Tell me what to do?"
                              + "\n1-Add employee:"
                              + "\n2-Remove employee:"
                              + "\n3-Display employee details by id:"
                              + "\n4-Display all employees details:"
                              + "\n5-Update employee detail:"
                              + "\n6-Exit employee menu:";
    public static final String PROJECT_OPTIONS = "Tell me what to do?"
                              + "\n1-Create new project:"
                              + "\n2-Remove project:"
                              + "\n3-Display project details by id:"
                              + "\n4-Display all projects details:"
                              + "\n5-Update project details"
                              + "\n6-assign employee to a project:"
                              + "\n7-Remove employee from project"
                              + "\n8-display employees for particular project"
                              + "\n9-Exit the project menu:";
    public static final String UPDATE_OPTIONS = "'s Update option "
                                   + "\n1-Update employee name :" 
                                   + "\n2-Update employee mail:"
                                   + "\n3-Update employee designation :"
                                   + "\n4-update empoyee dob :"
                                   + "\n5- update employee address"
                                   + "\n6-go to another employee' update page :"
                                   + "\n7-Exit update menu :";
    public static final String NAME = "employeeName";
    public static final String ID = "employeeId";
    public static final String MAIL = "employeeMail";
    public static final String DESIGNATION = "employeeDesignation";
    public static final String DOB = "employeeDob";
    public static final String EMPLOYEE_ID_INPUT = 
                                            "Enter employee id eg : E-12 :";
    public static final String EMPLOYEE_NAME_INPUT = "Enter the employee name:";
    public static final String EMPLOYEE_MAIL_INPUT = "Enter the employee mail:";
    public static final String EMPLOYEE_DESIGNATION_INPUT = 
                                             "Enter the employee designation :";
    public static final String EMPLOYEE_DOB_INPUT = "Enter the employee dob :";
    public static final String DESIGNATION_UPDATION = 
                                    "Employee designation updation successfull";
    public static final String MAIL_UPDATION = 
                                           "Employee mail updation successfull";     
    public static final String DOB_UPDATION = 
                                            "Employee dob updation successfull";
    public static final String NAME_UPDATION = 
                                           "Employee NAME updation successfull";
    public static final String INVALID_MAIL = "Employee mail or id is invalid ";
    public static final String INVALID_DESIGNATION = 
                                       "Employee designation or id is invalid ";
    public static final String INVALID_DOB = "Employee dob or id is invalid ";
    public static final String INVALID_NAME = "Employee name or id is invalid ";
    public static final String EMPLOYEE_NOT_EXIST = "Employee not exist";
    public static final String EMPLOYEE_DELETION = 
                                                 "Employee deletion successful";
    public static final String EMPLOYEE_CREATION = 
                                                 "Employee creation successful";
    public static final String INVALID_EMPLOYEE_DETAILS =
                                                 "Employee detail is not valid";
    public static final String PROJECT_NAME = "project name";
    public static final String PROJECT_ID = "project id";
    public static final String PROJECT_CLIENT = "project client";
    public static final String PROJECT_CREATION = "Project created successfuly";
    public static final String INVALID_PROJECT_DETAILS = 
                                          "Invalid details or id already exist";
    public static final String PROJECT_DELETION = 
                                                 "Project deleted successfully";
    public static final String INVALID_PROJECT_NAME = 
                                       "Invalid project name or id not present";
    public static final String INVALID_PROJECT_CLIENT = 
                                     "Invalid project client or id not present";
    public static final String INVALID_PROJECT_ID = 
                                         "Invalid project id or id not present";
    public static final String PROJECT_UPDATE_PAGE = 
                                             "Updation page of project with id"; 
    public static final String PROJECT_UPDATE_OPTIONS =  "\n1-Update name :" 
                                                       + "\n2-Update client:"
                                                       + "\n3-go to another "
                                                           + "page update page"
                                                       + "\n4-quit :";    
    public static final String PROJECT_NAME_UPDATION = 
                                            "Project name updation successfull";
    public static final String PROJECT_CLIENT_UPDATION = 
                                          "Project client updation successfull";
    public static final String SHOW_ALL_EMPLOYEES = 
                                "Do you want me to show all the employees(y/n)";
    public static final String PROJECT_ID_INPUT = 
                                    "Enter the 2 digit number for project id :";
    public static final String PROJECT_NAME_INPUT = "Enter the project name :";
    public static final String PROJECT_CLIENT_INPUT = 
                                                   "Enter the client id for "
                                                     +"this project:";
    public static final String PROJECT_DOMAIN_INPUT = "Enter the project domain";
    public static final String EMPLOYEE_ASSIGNMENT = 
                                              "Employee assignment successfull";
    public static final String EMPLOYEE_ERROR = 
                         "The employee not exist or already assigned a project";
    public static final String EMPLOYEE_NOT_IN_PROJECT = 
                      "The employee not in the project or employee not present";
    public static final String ASSIGNED_EMPLOYEES = 
                                          "Empoyees assigned for project id : ";
    public static final String ANOTHER_EMPLOYEE_ASSIGNMENT = 
                         "Assign another employee for this project id (y/n) : ";
    public static final String SQL_EXCEPTION = 
                                       "Cant't connect to database\nTry again;";
    public static final String NO_PROJECTS = "No projects found";
    public static final String NO_EMPLOYEES = "No employees found"; 
    public static final String ERROR_MESSAGE_FOR_USER =
                                                    "oops!Something went wrong";
    public static final String CLIENT_ID_INPUT = 
                                            "Enter the 2 digit number for id :";
    public static final String CLIENT_NAME_INPUT = "Enter the client name:";
    public static final String CLIENT_MAIL_INPUT = "Enter the client mail:";
    public static final String CLIENT_CONTACT_NO_INPUT = 
                                             "Enter the client contact no. :";
    public static final String CLIENT_PROJECT_ID_INPUT = "Enter the client project id :";
    public static final String CLIENT_CONTACT_NO_UPDATION = 
                                    "Client contact no. updation successfull";
    public static final String CLIENT_MAIL_UPDATION = 
                                           "Client mail updation successfull";     
    public static final String CLIENT_PROJECT_ID_UPDATION = 
                                            "Client project id updation successfull";
    public static final String CLIENT_NAME_UPDATION = 
                                           "Client NAME updation successfull";
    public static final String INVALID_CLIENT_MAIL = "Client mail or id is invalid ";
    public static final String INVALID_CLIENT_CONTACT_NO = 
                                       "Client contact no. or id is invalid ";
    public static final String INVALID_CLIENT_PROJECT_ID = "Client project id or id is invalid ";
    public static final String INVALID_CLIENT_NAME = "Client name or id is invalid ";
    public static final String CLIENT_NOT_EXIST = "Client not exist";
    public static final String CLIENT_DELETION = 
                                                 "Client deletion successful";
    public static final String CLIENT_CREATION = 
                                                 "Client creation successful";
    public static final String INVALID_CLIENT_DETAILS =
                                                 "Client detail is not valid";
    public static final String CLIENT_NAME = "clientName";
    public static final String CLIENT_ID = "clientId";
    public static final String CLIENT_MAIL = "clientMail";
    public static final String CLIENT_CONTACT_NO = "clientDesignation";
    public static final String CLIENT_PROJECT_ID = "clientDob";
    public static final String CLIENT_OPTIONS = "Tell me what to do?"
                              + "\n1-Add client:"
                              + "\n2-Remove client:"
                              + "\n3-Display client details by id:"
                              + "\n4-Display all clients details:"
                              + "\n5-Update client detail:"
                              + "\n6-Exit client menu:";
    public static final String CLIENT_UPDATE_OPTIONS = "'s Update options"                                                               + "whose id "
                                   + "\n1-Update client name :" 
                                   + "\n2-Update client mail:"
                                   + "\n3-Update client contact no :"
                                   + "\n4-update empoyee project id :"
                                   + "\n5- update client address"
                                   + "\n6-go to another client' update page :"
                                   + "\n7-Exit update menu :";
   public static final String PROJECT_DOMAIN_INPUT_STATEMENT = 
                                                   "Enter the project domain";
}
