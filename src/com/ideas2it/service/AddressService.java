package com.ideas2it.service;

import java.util.List;

import com.ideas2it.exception.MyException;
import com.ideas2it.model.Address;

/** 
 * <p>
 * Defines the methods for storing , retrieving, removing and updating address
 * </p>
 */ 
public interface AddressService {
    /** 
     * <p>
     * Adds the address to the given id
     * </p>
     * @param  id   
     *            id of employee/client , an unique entity for identification
     * @param  addressType 
     *            denotes permanent/ current address as "P"/"S"
     * @param  doorNo
     *            door no. contains alphanumeric and special characters    
     * @param  landmark 
     *            can have alphanumerics and other values
     * @param   street 
     *            can have alphanumerics and special characters
     * @param   city
     *            can hava alphabets and multi words
     * @param   district 
     *            can have alphabets and multiple words are allowed
     * @param   state 
     *             can have alphabets and allowed mutiple words
     * @param   country
     *             can have alphabets and multiple words
     * @param   postcode
     *             can have numerals and multiple words not allowed
     */
    public void addAddress(String id, String addressType, String doorNo,
                                        String landMark, String street,
                                        String city, String district,
                                        String state, String country, 
                                        String postCode) throws MyException;
    
  
    /**
     * <p>
     * Deletes  the address corresponds to the given id
     * </p> 
     * @param   id   an unique entity for identification
     */                                  
    public boolean removeAddressById(String id) throws MyException;
    
    /**
     * <p>
     * Updates the address for given id
     * </p>
     * @param  id   
     *            id of employee/client , an unique entity for identification
     * @param  addressType 
     *            denotes permanent/ current address as "P"/"S"
     * @param  doorNo
     *            door no. contains alphanumeric and special characters    
     * @param  landmark 
     *            can have alphanumerics and other values
     * @param   street 
     *            can have alphanumerics and special characters
     * @param   city
     *            can hava alphabets and multi words
     * @param   district 
     *            can have alphabets and multiple words are allowed
     * @param   state 
     *             can have alphabets and allowed mutiple words
     * @param   country
     *             can have alphabets and multiple words
     * @param   postcode
     *             can have numerals and multiple words not allowed
     */
    public boolean updateAddressById(String id, String addressType, 
                              String doorNo, String landMark, String street,
                              String city, String district, String state,
                              String country, String postCode)
                                                           throws MyException;
      
    /**
     * <p>
     * Gets all the address for given id
     * </p>   
     * @return   returns all address as list or null if no addresses were stored
     */
    public List<Address> getAllAddresses() throws MyException;
    
    /**
     * <p>
     * Gets an address for the given id
     * </p>
     * @param  id an unique entity for identification
     * @return permanent and current address for the id provided
     */
    public List<Address> getAddressById(String id) throws MyException;
}
