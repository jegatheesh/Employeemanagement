package com.ideas2it.service;

import java.util.List;

import com.ideas2it.exception.MyException;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;

/**
 * Stores all the project details 
 * update previously stored project details
 * delete an project detail
 * provide the details of project asked by the project id
 * provides all the project details stored 
 * author Jegatheesh Palanisamy
 * 26/07/2017
 */
public interface ProjectService  {
    
    /**
     * <p>
     * Creates new project that holds the details id, name, client
     * </p> 
     * @param    id          project id, a unique entity for identification 
     * @param    name        name of the project not necessary to be unique
     * @param    client      project client not necessarily to be unique
     * @return   returns true if created the project else false
     */
    public void createProject(String id, String name, String clientId,
                                          String projectDomain) throws 
                                                           MyException;
     
 
    /**
     * <p>
     * Deletes particular project detail by project id 
     * </p>
     * @param   id   project id, a unique entity for identification
     * @return       returns true if removed the project else false
     */    
     
    public void removeProjectById(String id) throws MyException;
 
    /**
     * <p>
     * Gets all projects' details and returns 
     * </p>
     * @return   returns all the projects details (list) or null if there is 
     *           no project saved before or all projects are deleted priorly
     */ 
    public List<Project> getAllProjects() throws MyException;

    /**
     * <p>
     * Gets the details of particular project detail by project id 
     * </p>
     * @param     id      project id, a unique entity for identification
     * @return    project details of the project id returns null if there is no
     *            project presenr by the given id
     */
    public Project getProjectById(String id) throws MyException;

    /**
     * <p>
     * Replaces an projects old name by new one
     * </p>
     * @param    id          project id, a unique entity for identification
     * @param    newName     holds the project's new name
     * @return   returns true if updated else false
     */
    public void updateProjectName(String id, String newName) throws 
                                                           MyException;
  
    /**
     * <p>
     * Replaces an projects old client by new one
     * </p>
     * @param    id            project id, a unique entity for identification 
     * @param    newClient     holds the project's new client
     * @returns true if updated the client else false
     */  
    public void updateProjectDomain(String id, String newClient) throws 
                                                              MyException;

    
  
    /**
     * <p>
     * Assigns employee to the project provided both employee id and project id
     * </p>
     * @param     employeeId   employee id, a unique entity for identification
     * @param     projectId    project id, a unique entity for identification
     * @return     returns true if employee is assigned else false
     */
    public void assignEmployee(String employeeId, String projectId) 
                                                    throws MyException;
    
    /**
     * <p>
     * Gets  the employee' project id detail provided the employee id
     * </p>
     * @param     employeeId    employee id, a unique entity for identification
     * @return    returns the project id of employee whose id is passed
     */
    public String getEmployeeProjectId(String employeeId) throws
                                                           MyException;
    
    /**
     * <p>
     * Removes the employee from the project provide the employee id
     * and the project id
     * </p>
     * @param     employeeId     employee id who is to be removed from project
     * @param     projectId      project id from which employee is removed
     * @return    returns true if remove is success else false
     */
    public void removeEmployeeFromProject(String employeeId, 
                                             String projectId)
                                                    throws MyException;
                                                    
    /**
     * <p>
     * Gets all the employees details for particular project provided 
     * the project id
     * </p>
     * @param    projectId    project id, a unique entity for identification
     * @return   returns all the employee name as list
     */
    public List<Employee> getAllEmployeesByProjectId(String projectId) throws
                                                           MyException;
  
    /**
     * <p>
     * Checks for the presence of the project by the project id
     * </p>
     * @param    id   project id, a unique entity for identification
     * @return   true if present else false
     */
    public boolean isProjectExist(String id) throws MyException;
}




