package com.ideas2it.service;

import java.util.List;

import com.ideas2it.exception.MyException;
import com.ideas2it.model.Employee;

/**
 * Stores all the employee details 
 * update previously stored employee details
 * delete an employee detail
 * provide the details of employee asked by the employee id
 * provides all the employee details stored 
 * author Jegatheesh Palanisamy
 * 26/07/2017
 */
public interface EmployeeService {

    /**
     * <p>
     * Adds new employee detail that holds the employee id, name, designation,
     * mail, dob
     * </p>
     * @param    id          id of employee ,an unique entity for identification
     * @param    name        employee name, not necessary to be unique 
     * @param    designation employee designation, not necessary to be unique 
     * @param    mail        employee mail, an unique entity for each employee 
     * @param    dob         employee dob, not necessarily unique
     */
    public void insertEmployee(String id, String name, String designation,
                                  String mail, String dob) throws MyException;

    /**
     * <p>
     * Deletes particular employee detail by employee id
     * </p> 
     * @param   id   id of employee, an unique entity for identification
     */    
    public boolean removeEmployeeById(String id) throws MyException;

    /**
     * <p>
     * Gets all employees' details  
     * </p>
     * @return   returns all the employees details (list) or null if there is 
     *           no employee stored or all details deleted priorly 
     */ 
    public List<Employee> getAllEmployees() throws MyException;

    /**
     * <p>
     * Gets the details of particular employee detail by employee id 
     * </p>
     * @param     id      id of employee, an unique entity for identification
     * @return    employee details of the employee id or null of the employee
     *            whose id provided not exist
     */
    public Employee getEmployeeById(String id) throws MyException;

    /**
     * <p>
     * Replaces an employees old age by new one
     * </p>
     * @param      id     id of employee, an unique entity for identification
     * @param    newDob   holds the employee's newDob
     */
    public boolean updateEmployeeDob(String id, String newDob) throws 
                                                                  MyException;
    
    /**
     * <p>
     * Replaces an employees old name by new one
     * </p>
     * @param    id        id of employee, an unique entity for identification
     * @param    newName     holds the employee's new name
     */
    public boolean updateEmployeeName(String id, String newName) throws
                                                           MyException;
  
    /**
     * <p>
     * Replaces an employees old designation by new one
     * </p>
     * @param    id                 id of employee, an unique entity for 
     *                                                    identification
     * @param    newDesignation     holds the employee's new designation
     */
    public boolean updateEmployeeDesignation(String id, String newDesignation)
                                                           throws MyException;

    /**
     * <p>
     * Replaces an employees old mail by new one
     * </p>
     * @param    id        id of employee, an unique entity for identification
     * @param    newMail   holds the employee's new mail
     */  
    public boolean updateEmployeeMail(String id, String newMail) throws
                                                                  MyException;
        
    /**
     * <p>
     * Checks for employee detail existence, provided the employee id
     * </p>
     * @param    id  id of employee, an unique entity for identification 
     * @return   true if present else false
     */
    public boolean checkEmployeeExistence(String id) throws MyException;   
    
    /**
     * <p>
     * Find which details of employee are invalid
     * </p>
     * @return   an array holds binary values digit 1 in each position 
     * represents corresponding value is invalid
     */
    public int[] findValidEmployeeDetails(String id, String name, 
                                  String designation, String mail, String dob);                                                        
}




