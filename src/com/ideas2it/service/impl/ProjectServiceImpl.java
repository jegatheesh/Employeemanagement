package com.ideas2it.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import com.ideas2it.dao.ProjectDao;
import com.ideas2it.dao.impl.ProjectDaoImpl;
import com.ideas2it.exception.MyException;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.util.CommonUtil;

/**
 * Stores all the project details 
 * update previously stored project details
 * delete an project detail
 * provide the details of project asked by the project id
 * provides all the project details stored 
 * author Jegatheesh Palanisamy
 * 26/07/2017
 */
public class ProjectServiceImpl implements ProjectService  {
    ProjectDao projectDao = new ProjectDaoImpl();
    
    /**
     * @see package com.ideas2it.service; #public boolean createProject(
     *                                  String id, String name, String client)
     */
    public void createProject(String id, String name, String clientId, 
                                  String projectDomain) throws  MyException {                                                          
        if (!isValidProjectId(id)) {
            throw new MyException ("Entered project id not valid");
        }             
        
        if (!isValidClientId(clientId)) {
            throw new MyException("Entered client id not valid");
        }                    
        
        if (isProjectExist(id)) {
            throw new MyException ("Entered project id already exist");
        }                                                                      
        projectDao.insertProject(id, name, clientId, projectDomain);        
    }                                 

    /**
     * @see package com.ideas2it.service; #public boolean removeProjectById(
     *                                                                String id)
     */    
    public void removeProjectById(String id) throws MyException {
        if (!isValidProjectId(id)) {
            throw new MyException("ProjectId not valid");
        } 
        if (!isProjectExist(id)) {
            throw new MyException("Project id not present");
        }    
        projectDao.removeProjectById(id);        
    }

    /**
     * @see package com.ideas2it.service; #public List<Project> getAllProjects()
     */ 
    public List<Project> getAllProjects() throws MyException {
        return projectDao.getAllProjects(); 
    }

    /**
     * @see package com.ideas2it.service; #public Project getProjectById(
     *                                                              String id) 
     */
    public Project getProjectById(String id) throws MyException {
        return projectDao.getProjectById(id);
    }  

    /**
     * @see package com.ideas2it.service; #public boolean updateProjectName(
     *                                                String id, String newName)
     */
    public void updateProjectName(String id, String newName) throws 
                                                               MyException {
        if (!isValidProjectId(id)) {
            throw new MyException("Project id not valid");
        } 
        if (!isProjectExist(id)) {
            throw new MyException("Project id Not exist");
        }
        projectDao.updateProjectDetail("PROJECT_NAME", newName, id); 
    }
  
    /**
     * @see package com.ideas2it.service; #public boolean updateProjectClient(
     *                                            String id, String newClient)
     */  
    public void updateProjectDomain(String id, String newProjectDomain) throws 
                                                              MyException {
        if (!isValidProjectId(id)) {
            throw new MyException("Project id not valid");
        } 
        if (isProjectExist(id)) {
            throw new MyException("Project id not exist");
        }
        projectDao.updateProjectDetail("PROJECT_DOMAIN", newProjectDomain, id);
    }  

    /*
     * <p>
     * Validates the input id for the necessary format
     * </p>
     * @param  id      project id, a unique entity for identification
     * @return returns true if pattern matches else false
     */
    private boolean isValidProjectId(String id) {
        return CommonUtil.isProjectIdValid(id);
    }
    
    /*
     * <p>
     * Validates the input id for the necessary format
     * </p>
     * @param  id      client id, a unique entity for identification
     * @return returns true if pattern matches else false
     */
    private boolean isValidClientId(String id) {
        return CommonUtil.isClientIdValid(id);
    }
    
    private boolean isValidEmployeeId(String employeeId) {
        return CommonUtil.isEmployeeIdValid(employeeId);
    }
    
    /**
     * @see package com.ideas2it.service; #isProjectExist(String id)
     */
    public boolean isProjectExist(String id) throws MyException {                                            
        if (null == getProjectById(id)) {
            return false;
        }
        return true;
    }   
    
    /**
     * @see package com.ideas2it.service; #public boolean assignEmployee(
     *                                     String employeeId, String projectId)
     */
    public void assignEmployee(String employeeId, String projectId) 
                                                    throws MyException {
        EmployeeService employeeService = new EmployeeServiceImpl();
        if (!employeeService.checkEmployeeExistence(employeeId)) {
            throw new MyException("Employee not exist");
        }
        if (null != getEmployeeProjectId(employeeId)) {  
            throw new MyException("Employee already assigned to "
                                                          + "another project");
        }        
        projectDao.assignEmployee(employeeId, projectId);  
    }
    
    /**
     * @see package com.ideas2it.service; #public String getEmployeeProjectId(
     *                                                        String employeeId)
     */
    public String getEmployeeProjectId(String employeeId) throws
                                                           MyException {     
        return projectDao.getEmployeeProjectId(employeeId);  
    }
    
    /**
     * @see package com.ideas2it.service; 
     *              #public boolean removeEmployeeFromProject(String employeeId, 
     *                                                       String projectId)
     */
    public void removeEmployeeFromProject(String employeeId, 
                                             String projectId)
                                                    throws MyException { 
        if (!isValidProjectId(projectId)) {
            throw new MyException("Project id not valid");
        } 
        if (!isValidEmployeeId(employeeId)) {
            throw new MyException("Employee id notvalid");
        }
        EmployeeService employeeService = new EmployeeServiceImpl();
        if (!employeeService.checkEmployeeExistence(employeeId)) {
            throw new MyException("Employee not exist");
        }
        if (!getEmployeeProjectId(employeeId).equals(projectId)) {
            throw new MyException("Employee is not assigned to this project");
        }
        projectDao.removeEmployeeFromProject(employeeId);  
    }
    
    /**
     * @see package com.ideas2it.service;  
     *     #public List<Employee> getAllEmployeesByProjectId(String projectId)
     */
    public List<Employee> getAllEmployeesByProjectId(String projectId) throws
                                                           MyException {
        if (isProjectExist(projectId)) {
            return projectDao.getAllEmployeesByProjectId(projectId);  
        }
        return null;
    }
}




