package com.ideas2it.service.impl;

import java.util.List;
import java.util.Scanner;

import com.ideas2it.dao.EmployeeDao;
import com.ideas2it.dao.impl.EmployeeDaoImpl;
import com.ideas2it.exception.MyException;
import com.ideas2it.model.Employee;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.util.CommonUtil;

/**
 * Stores all the employee details 
 * update previously stored employee details
 * delete an employee detail
 * provide the details of employee asked by the employee id
 * provides all the employee details stored 
 * author Jegatheesh Palanisamy
 * 26/07/2017
 */
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDao employeeDao = new EmployeeDaoImpl();

    /**
     * @see com.ideas2it.service.EmployeeService; #public boolean insertEmployee
     *                              (String id, String name, String designation,
     *                                                  String mail, String dob)
     */
    public void insertEmployee(String id, String name, String designation,
                                  String mail, String dob) throws MyException {
        if (checkEmployeeExistence(id)) { 
            throw new MyException("Employee id already exist");
        }      
        employeeDao.insertEmployee(id, name, designation, mail, dob);
    }
     
    /**
     * <p>
     * Validates each employee details passed for necessary format
     * </p>
     * @return     returns true if each detail matches the format else false
     */
    private boolean isValidEmployeeDetails(String id, String name,
                                           String designation, String mail,
                                                             String dob) {
        return isValidEmployeeName(name) &&
               isValidEmployeeId(id) &&
               isValidEmployeeDesignation(designation) &&
               isValidEmployeeMail(mail) &&
               isValidEmployeeDob(dob);                           
    }                                    

    /**
     * @see com.ideas2it.service.EmployeeService;
     *                           #public boolean removeEmployeeById(String id)
     */   
    public boolean removeEmployeeById(String id) throws MyException {
        if (isValidEmployeeId(id) && checkEmployeeExistence(id)) {
            employeeDao.removeEmployeeById(id);  
            return true; 
        }
        return false;        
    }

    /**
     * @see com.ideas2it.service.EmployeeService; 
     *                                 #public List<Employee> getAllEmployees()
     */ 
    public List<Employee> getAllEmployees() throws MyException {s
        return employeeDao.getAllEmployees();
    }

    /**
     * <p>
     * Gets the details of particular employee detail by employee id 
     * </p>
     * @param     id      id of employee, an unique entity for identification
     * @return    employee details of the employee id or null of the employee
     *            whose id provided not exist
     */
    public Employee getEmployeeById(String id) throws MyException {
        return employeeDao.getEmployeeById(id);
    }  

    /**
     * @see com.ideas2it.service.EmployeeService; 
     *              #public boolean updateEmployeeDob(String id, String newDob)
     */
    public boolean updateEmployeeDob(String id, String newDob) throws 
                                                           MyException {
        if (isValidEmployeeDob(newDob) && checkEmployeeExistence(id)) {
            employeeDao.updateEmployeeDetail("DOB", newDob, id);
            return true;
        }
        return false;
    }
    
    /**
     * @see com.ideas2it.service.EmployeeService; 
     *          #public boolean updateEmployeeName(String id, String newName)
     */
    public boolean updateEmployeeName(String id, String newName) throws
                                                           MyException {
       if(isValidEmployeeName(newName) && checkEmployeeExistence(id)) {
           employeeDao.updateEmployeeDetail("NAME", newName, id);
           return true;
       }
       return false;
    }
  
    /**
     * @see com.ideas2it.service.EmployeeService; 
     *               #public boolean updateEmployeeDesignation(
     *                                       String id, String newDesignation)
     */
    public boolean updateEmployeeDesignation(String id, String newDesignation)
                                                    throws MyException {
        if (isValidEmployeeDesignation(newDesignation)
                                         && checkEmployeeExistence(id)) {
            employeeDao.updateEmployeeDetail("DESIGNATION", newDesignation, id);
            return true;
        }
        return false;
    }

    /**
     * @see com.ideas2it.service.EmployeeService; 
     *             #public boolean updateEmployeeMail(String id, String newMail)
     */ 
    public boolean updateEmployeeMail(String id, String newMail) throws
                                                                  MyException {
        if (isValidEmployeeMail(newMail) && checkEmployeeExistence(id)) {
            employeeDao.updateEmployeeDetail("MAIL", newMail, id);     
            return true;
        }
        return false;
    }  

    /**
     * <p>
     * Validtates the input id for the necessary format
     * </p>
     * @param  id      id of employee, an unique entity for identification
     * @return returns true if format matches else false
     */
    private boolean isValidEmployeeId(String id) {
        return CommonUtil.isEmployeeIdValid(id);
    }
    
    /** 
     * <p>
     * Validates the input name for the necessary format
     * @param  name      name of the employee
     * @return returns true if format matches else false
     */    
    private boolean isValidEmployeeName(String name) {
        return CommonUtil.isNameValid(name);
    }

    /** 
     * <p>
     * Validates the input designation for the necessary format
     * </p>
     * @param  designation    designation of the employee
     * @return returns true if format matches else false
     */
    private boolean isValidEmployeeDesignation(String designation) {
        return CommonUtil.isNameValid(designation);
    }

    /**
     * <p>
     * validates the input mail for the necessary format
     * </p>
     * @param  mail      mail of the employee
     * @return returns true if format matches else false
     */
    private boolean isValidEmployeeMail(String mail) {
        return CommonUtil.isMailValid(mail);
    }
    
    /**
     * <p>
     * validates the input dob for the necessary format
     * </p>
     * @param  dob      dob of the employee
     * @return returns true if format matches else false
     */
    private boolean isValidEmployeeDob(String dob) {
        return CommonUtil.isDobValid(dob);
    }
        
    /**
     * @see com.ideas2it.service.EmployeeService; 
     *                   #public boolean checkEmployeeExistence(String id)
     */
    public boolean checkEmployeeExistence(String id) throws 
                                                           MyException {                                                
        if (null == getEmployeeById(id)) {
            return false;
        }
        return true;
    }   
    
    /**
     * @see com.ideas2it.service.EmployeeService; 
     *                   #findValidEmployeeDetails(String id, String name, 
     *                            String designation, String mail, String dob))
     */
    public int[] findValidEmployeeDetails(String id, String name, 
                                  String designation, String mail, String dob) {
        int[] validFields = new int[5];
        if (!isValidEmployeeId(id)) {
            validFields[0] = 1;
        }
        if (!isValidEmployeeName(name)) {
            validFields[1] = 1;
        }
        if (!isValidEmployeeDesignation(designation)) {
            validFields[2] = 1;
        }
        if (!isValidEmployeeMail(mail)) {
            validFields[3] = 1;
        }
        if (!isValidEmployeeDob(dob)) {
            validFields[4] = 1;
        }
        return validFields;                                       
    }
}
