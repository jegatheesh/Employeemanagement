package com.ideas2it.service.impl;

import java.util.List;
import java.util.Scanner;

import com.ideas2it.dao.ClientDao;
import com.ideas2it.dao.impl.ClientDaoImpl;
import com.ideas2it.exception.MyException;
import com.ideas2it.model.Client;
import com.ideas2it.service.ClientService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.service.impl.ProjectServiceImpl;
import com.ideas2it.util.CommonUtil;

/**
 * Stores all the client details 
 * update previously stored client details
 * delete an client detail
 * provide the details of client asked by the client id
 * provides all the client details stored 
 * author Jegatheesh Palanisamy
 * 26/07/2017
 */
public class ClientServiceImpl implements ClientService {
    private ClientDao clientDao = new ClientDaoImpl();

    /**
     * @see com.ideas2it.service.ClientService; #public boolean insertClient
     *                              (String id, String name, String contactNo,
     *                                           String mail, String projectId)
     */
    public void insertClient(String id, String name, String contactNo,
                                  String mail) throws MyException {
        ProjectService projectService = new ProjectServiceImpl();
        if (checkClientExistence(id)) {
            throw new MyException("Client id already exist");
        }  
        clientDao.insertClient(id, name, contactNo, mail);
            
    }                                    

    /**
     * @see com.ideas2it.service.ClientService;
     *                           #removeClientById(String id)
     */   
    public void removeClientById(String id) throws MyException {
        if (!isValidClientId(id)) {
            throw new MyException("Entered client id not valid");
        } 
        System.out.println(checkClientExistence(id));
        if (!checkClientExistence(id)) {
            throw new MyException("Client id not exist"); 
        }
        clientDao.removeClientById(id);
    }
    
    

    /**
     * @see com.ideas2it.service.ClientService; 
     *                                 #public List<Client> getAllClients()
     */ 
    public List<Client> getAllClients() throws MyException {
        return clientDao.getAllClients();
    }

    /**
     * <p>
     * Gets the details of particular client detail by client id 
     * </p>
     * @param     id      id of client, an unique entity for identification
     * @return    client details of the client id or null of the client
     *            whose id provided not exist
     */
    public Client getClientById(String id) throws MyException {
        return clientDao.getClientById(id);
    }  

    /**
     * @see com.ideas2it.service.ClientService; 
     *              #public boolean updateClientProjectId(String id, String newProjectId)
     */
    public void updateClientMail(String id, String newMail) throws MyException{
        if (!isValidClientId(id)) {
            throw new MyException("Entered client id not valid");
        }                                                    
        if (!isValidClientMail(newMail)) {
            throw new MyException("Entered mail not valid");
        } 
        if (!checkClientExistence(id)) {
            throw new MyException("Client id not exist");        
        }
        clientDao.updateClientDetail("MAIL", newMail, id);
    }
    
    /**
     * @see com.ideas2it.service.ClientService; 
     *          #public boolean updateClientName(String id, String newName)
     */
    public void updateClientName(String id, String newName) throws
                                                           MyException {
       if (!isValidClientId(id)) {
            throw new MyException("Entered client id not valid");
        }                       
       if (!isValidClientName(newName)) {
            throw new MyException("Entered name not valid");
       } 
       if (!checkClientExistence(id)) {
           throw new MyException("Client id not exist");
       }
       clientDao.updateClientDetail("CLIENT_NAME", newName, id);
    }
  
    /**
     * @see com.ideas2it.service.ClientService; 
     *               #public boolean updateClientContactNo(
     *                                       String id, String newContactNo)
     */
    public void updateClientContactNo(String id, String newContactNo)
                                                    throws MyException {
        if (!isValidClientId(id)) {
            throw new MyException("Entered client id not valid");
        }
        if (isValidClientContactNo(newContactNo)) {
            throw new MyException("Entered contact no not valid");
        }
        if (!checkClientExistence(id)) {
           throw new MyException("Client id not exist");
        }
        clientDao.updateClientDetail("CONTACT_NO", newContactNo, id);
    }

    /**
     * @see com.ideas2it.service.ClientService; 
     *             #public boolean updateClientProjectId(String id, String newProjectId)
     */ 
    public void updateClientProjectId(String id, String newProjectId) throws
                                                                  MyException {
        if (!isValidClientId(id)) {
            throw new MyException("Entered client id not valid");
        }
        if (!checkClientExistence(id)) {
           throw new MyException("Client id not exist");
        }              
        if (isValidClientProjectId(newProjectId)) {
            throw new MyException("Entered project id not valid");
        } 
        clientDao.updateClientDetail("PROJECT_ID", newProjectId, id);     
    }  

    /**
     * <p>
     * Validtates the input id for the necessary format
     * </p>
     * @param  id      id of client, an unique entity for identification
     * @return returns true if format matches else false
     */
    private boolean isValidClientId(String id) {
        return CommonUtil.isClientIdValid(id);
    }
    
    /**
     * <p>
     * Validates the input string fot the necaessary format
     * </p>
     * @param  id      contact no. of client, an unique entity 
     */
     private boolean isValidClientContactNo(String contactNo) {
         return CommonUtil.isValidContactNo(contactNo);
     }
    /** 
     * <p>
     * Validates the input name for the necessary format
     * @param  name      name of the client
     * @return returns true if format matches else false
     */    
    private boolean isValidClientName(String name) {
        return CommonUtil.isNameValid(name);
    }

    /** 
     * <p>
     * Validates the input contactNo for the necessary format
     * </p>
     * @param  contactNo    contactNo of the client
     * @return returns true if format matches else false
     */
    private boolean isValidClientMail(String mail) {
        return CommonUtil.isMailValid(mail);
    }
    
    /**
     * <p>
     * validates the input projectId for the necessary format
     * </p>
     * @param  projectId      projectId of the client
     * @return returns true if format matches else false
     */
    private boolean isValidClientProjectId(String projectId) {
        return CommonUtil.isProjectIdValid(projectId);
    }
        
    /**
     * @see com.ideas2it.service.ClientService; 
     *                   #public boolean checkClientExistence(String id)
     */
    public boolean checkClientExistence(String id) throws MyException { 
        if (null == getClientById(id)) {
            return false;
        }
        return true;
    }   
    
    /**
     * @see com.ideas2it.service.ClientService; 
     *                   #findValidClientDetails(String id, String name, 
     *                            String designation, String mail, String dob)
     */
    public int[] findValidClientDetails(String id, String name, 
                                  String contactNo, String mail) {
        int[] validFields = new int[4];
        if (!isValidClientId(id)) {
            validFields[0] = 1;
        }
        if (!isValidClientName(name)) {
            validFields[0] = 1;
        }
        if (!isValidClientContactNo(contactNo)) {
            validFields[0] = 1;
        }
        if (!isValidClientMail(mail)) {
            validFields[0] = 1;
        }
        return validFields;                
    }
}




