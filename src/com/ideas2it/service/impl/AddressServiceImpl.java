package com.ideas2it.service.impl;

import java.util.List;

import com.ideas2it.util.CommonUtil;
import com.ideas2it.dao.AddressDao;
import com.ideas2it.dao.impl.AddressDaoImpl;
import com.ideas2it.model.Address;
import com.ideas2it.service.AddressService;
import com.ideas2it.exception.MyException;

public class AddressServiceImpl implements AddressService {
    AddressDao addressDao = new AddressDaoImpl();
    
    /**
     * @see com.ideas2it.service.AddressService; 
     *      addAddress(String id, String addressType, String doorNo,
     *                                   String landMark, String street,
     *                                   String city, String district,
     *                                   String state, String country, 
     *                                   String postCode)
     */
    public void addAddress(String id, String addressType, String doorNo,
                                        String landMark, String street,
                                        String city, String district,
                                        String state, String country, 
                                        String postCode) throws MyException {
        addressDao.insertAddress(id, addressType, doorNo, landMark, street, 
                                     city, district, state, country, postCode);
        
    }
    
    /**
     * @see com.ideas2it.service.AddressService; #removeAddressById(String id)
     */
    public boolean removeAddressById(String id) throws MyException {
        if (true) {
            addressDao.removeAddressById(id);
            return true; 
        }
        return false;
    }
    
    /**
     * <p>
     * Validates the id for either client or project
     * </p>
     * @param   id  id of employee/client an unique entity for identification
     * @return  return true if it is valid else false
     */
    private boolean isIdValid(String id) {
        return CommonUtil.isIdValid(id);
    }
    
    /**
     * @see com.ideas2it.service.AddressService; 
     *                      updateAddressById(String id,String addressType, 
     *                         String doorNo, String landMark,
     *                         String street, String city, String district,
     *                         String state, String country, String postCode)
     */ 
    public boolean updateAddressById(String id,String addressType, String doorNo, String landMark,
                              String street, String city, String district,
                              String state, String country, String postCode)
                                                           throws MyException {
        if (true) {
            addressDao.updateAddressById(id, addressType, doorNo, landMark, street, city, 
                                           district, state, country, postCode);
            return true;
        }
        return false;
    }
    
    /**
     * @see com.ideas2it.service.AddressService; #getAllAddresses()
     */
    public List<Address> getAllAddresses() throws MyException {
        return addressDao.getAllAddresses();
    }
    
    /**
     * @see com.ideas2it.service.AddressService; #getAddressById(String id)
     */
    public List<Address> getAddressById(String id) throws MyException {
        return addressDao.getAddressById(id);
    }
}
