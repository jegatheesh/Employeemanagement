package com.ideas2it.service;

import java.util.List;

import com.ideas2it.exception.MyException;
import com.ideas2it.model.Client;


/**
 * Stores all the client details 
 * update previously stored client details
 * delete an client detail
 * provide the details of client asked by the client id
 * provides all the client details stored 
 * author Jegatheesh Palanisamy
 * 26/07/2017
 */
public interface ClientService {
    
    /**
     * <p>
     * Adds new client detail that holds the client id, name, contact no,
     * mail, project id
     * </p>
     * @param    id          id of client ,an unique entity for identification
     * @param    name        client name, not necessary to be unique 
     * @param    contactNo   client contactNo, not necessary to be unique 
     * @param    mail        client mail, an unique entity for each client 
     * @param    projectid   client projectId,not necessarily unique
     */
    public void insertClient(String id, String name, String contactNo,
                                  String mail) throws MyException;                                         

    /**
     * <p>
     * Deletes particular client detail by client id
     * </p> 
     * @param   id   id of client, an unique entity for identification
     */    
    public void removeClientById(String id) throws MyException;
    
    /**
     * <p>
     * Gets all clients' details  
     * </p>
     * @return   returns all the clients details (list) or null if there is 
     *           no client stored or all details deleted priorly 
     */  
    public List<Client> getAllClients() throws MyException;

    /**
     * <p>
     * Gets the details of particular client detail by client id 
     * </p>
     * @param     id      id of client, an unique entity for identification
     * @return    client details of the client id or null of the client
     *            whose id provided not exist
     */
    public Client getClientById(String id) throws MyException;

    /**
     * <p>
     * Replaces an clients old mail by new one
     * </p>
     * @param    id        id of client, an unique entity for identification
     * @param    newMail   holds the client's new mail
     */
    public void updateClientMail(String id, String newMailId) throws 
                                                           MyException;
        
    
    /**
     * <p>
     * Replaces an clients old name by new one
     * </p>
     * @param    id        id of client, an unique entity for identification
     * @param    newName     holds the client's new name
     */
    public void updateClientName(String id, String newName) throws
                                                           MyException;
  
    /**
     * <p>
     * Replaces an clients old contact no by new one
     * </p>
     * @param    id                 id of client, an unique entity for 
     *                                                    identification
     * @param    newContact no     holds the client's new contact no
     */
    public void updateClientContactNo(String id, String newContactNo)
                                                    throws MyException;
        
    /**
     * <p>
     * Checks for client detail existence, provided the client id
     * </p>
     * @param    id  id of client, an unique entity for identification 
     * @return   true if present else false
     */
    public boolean checkClientExistence(String id) throws MyException;
    
    /**
     * <p>
     * Finds the client details which are valid
     * </p>
     * @Return   returns an int that denotes tha valid and invalid details
     */
     public int[] findValidClientDetails(String id, String name, 
                                       String contactNo, String mail);                                                      
}



