package com.ideas2it.dao.impl;

import java.sql.Connection;
import com.ideas2it.dao.EmployeeDao;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.exception.MyException;
import com.ideas2it.database.DatabaseConnectivity;
import com.ideas2it.logs.LogManager;
import com.ideas2it.model.Employee;

/**
 * Inserts , updates, deletes the entrys in the employee table
 */
public class EmployeeDaoImpl implements EmployeeDao {
    private static final String EMPLOYEE_TABLE_NAME = "EMPLOYEE";
    private static final String EMPLOYEE_ID= "EMPLOYEE_ID";
    private static final String EMPLOYEE_NAME = "NAME";
    private static final String EMPLOYEE_MAIL = "MAIL";
    private static final String EMPLOYEE_DESIGNATION = "DESIGNATION";
    private static final String EMPLOYEE_DOB = "DOB";
    private static final String COLUMN_FOR_PROJECT_ID = "PROJECT_ID";
           
    /**
     * @see com.ideas2it.dao.EmployeeDao   #insertEmployee(String id,
     *                                      String name, String designation,
     *                                              String mail, String dob) 
     */
    public void insertEmployee(String id, String name, String designation,
                                  String mail, String dob) throws MyException {
        DatabaseConnectivity databaseConnectivity;
        Connection connection;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(
                                      "INSERT INTO " + EMPLOYEE_TABLE_NAME + "(" 
                                                  + EMPLOYEE_ID+"," 
                                                  + EMPLOYEE_NAME + "," 
                                                  + EMPLOYEE_MAIL + ","
                                                  + EMPLOYEE_DESIGNATION + ","
                                                  + EMPLOYEE_DOB 
                                                  + ") VALUES (?,?,?,?,?)");
            statement.setString(1, id);
            statement.setString(2, name);
            statement.setString(3, mail);
            statement.setString(4, designation);
            statement.setString(5, dob);
            statement.executeUpdate(); 
            statement.close();                         
        } catch(SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "Sql exception while "
                                   + "inserting employee :" + id, sqlException);
            throw new MyException("Can't add employee! Try again");
        } finally {
           try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(EmployeeDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }
    }
    
    /**
     * @see com.ideas2it.dao.EmployeeDao  #getEmployeeById(String id)
     */
    public Employee getEmployeeById(String id) throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(
                                                 "SELECT "
                                                  + EMPLOYEE_ID+"," 
                                                  + EMPLOYEE_NAME + "," 
                                                  + EMPLOYEE_MAIL + ","
                                                  + EMPLOYEE_DESIGNATION + ","
                                                  + EMPLOYEE_DOB + ","
                                                  + COLUMN_FOR_PROJECT_ID
                                              + " FROM " + EMPLOYEE_TABLE_NAME 
                                              + " WHERE " + EMPLOYEE_ID+ "= ?");
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery(); 
            if (resultSet.next()) {
                Employee employee = new Employee(
                                    resultSet.getString(EMPLOYEE_ID),
                                    resultSet.getString(EMPLOYEE_NAME),
                                    resultSet.getString(EMPLOYEE_DESIGNATION),
                                    resultSet.getString(EMPLOYEE_MAIL),
                                    resultSet.getString(EMPLOYEE_DOB));
                employee.setProjectId(resultSet.getString("PROJECT_ID")); 
                resultSet.close(); 
                statement.close();                                       
                return employee;                              
            }                        
        } catch(SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "SQL exception while "
                                      + "fetching detail employee of id :" + id, 
                                                                  sqlException);
            throw new MyException("Can't get the employee details!try again");
        }finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(EmployeeDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }      
        return null;
    }
    
    /**
     * @see com.ideas2it.dao.EmployeeDao  # removeEmployeeById(String id)
     */
    public void removeEmployeeById(String id) throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(
                                           "DELETE FROM " + EMPLOYEE_TABLE_NAME  
                                             + " WHERE "+ EMPLOYEE_ID+" = ?");
            statement.setString(1, id);
            statement.executeUpdate();   
            statement.close();                
        } catch(SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "Exception while deleting "
                                              + "employee :" +id, sqlException);
            throw new MyException("Can't remove the employee!Try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(EmployeeDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }      
    } 
    
    /**
     * @see com.ideas2it.dao.EmployeeDao  #  updateEmployeeDetail(
     *                                      String columnName, String newDetail,
     *                                                      String employeeId)
     */
    public void updateEmployeeDetail(String columnName, String newDetail,
                                     String employeeId) throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(
                                      "UPDATE " + EMPLOYEE_TABLE_NAME + " SET "
                                                   + columnName + " = ? WHERE "
                                                       + EMPLOYEE_ID+ " = ?");
   
            statement.setString(1,newDetail);
            statement.setString(2, employeeId);
            statement.executeUpdate();
            statement.close();
        } catch(SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "SQL exception thrown while "
                                              + "updating column " + columnName 
                                              + "of EMPLOYEE table where "
                                              + "employee id :" + employeeId,
                                              sqlException);
            throw new MyException("Can't update the employee!Try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(EmployeeDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }  
    }
    
    /**
     * @see com.ideas2it.dao.EmployeeDao;    # getAllEmployees()
     */
    public List<Employee> getAllEmployees() throws MyException {        
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
           LogManager.debug(EmployeeDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
           throw new MyException("Can,t connect!check connection");
        }
        try {
            Statement statement = connection.createStatement();
            List<Employee> employees = new ArrayList<Employee>();
            Employee employee = null;        
            ResultSet resultSet = statement.executeQuery("ELECT "
                                              + EMPLOYEE_ID+"," 
                                              + EMPLOYEE_NAME + "," 
                                              + EMPLOYEE_MAIL + ","
                                              + EMPLOYEE_DESIGNATION + ","
                                              + EMPLOYEE_DOB + ","
                                              + COLUMN_FOR_PROJECT_ID
                                              + " FROM " + EMPLOYEE_TABLE_NAME);
            while (resultSet.next()) {
                employee = new Employee(resultSet.getString(EMPLOYEE_ID),
                                        resultSet.getString(EMPLOYEE_NAME),
                                      resultSet.getString(EMPLOYEE_DESIGNATION),
                                        resultSet.getString(EMPLOYEE_MAIL),
                                        resultSet.getString(EMPLOYEE_DOB));
                employee.setProjectId(resultSet.getString("PROJECT_ID"));
                employees.add(employee);
            }
            resultSet.close();
            statement.close();
            return employees;
        } catch(SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "Exception while "
                                           + "fetching data from table :" 
                                           + EMPLOYEE_TABLE_NAME, sqlException);
            throw new MyException();
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(EmployeeDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }    
    }
}
