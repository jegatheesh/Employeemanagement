package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.dao.ClientDao;
import com.ideas2it.database.DatabaseConnectivity;
import com.ideas2it.exception.MyException;
import com.ideas2it.logs.LogManager;
import com.ideas2it.model.Client;

/**
 * Inserts , updates, deletes the entrys in the client table
 */
public class ClientDaoImpl implements ClientDao {
    private static final String PROJECT_TABLE = "PROJECT";
    private static final String CLIENT_TABLE = "CLIENT";
    private static final String CLIENT_ID= "CLIENT_ID";
    private static final String CLIENT_NAME = "CLIENT_NAME";
    private static final String CLIENT_MAIL = "MAIL";
    private static final String CLIENT_CONTACT_NO = "CONTACT_NO";
    private static final String PROJECT_ID = "PROJECT_ID";
    private static final String EMPLOYEE_TABLE = "EMPLOYEE"; 
           
    /**
     * @see com.ideas2it.dao.ClientDao   #public void insertClient(
     *                              String id, String name, String contact no,
     *                                              String mail, String project id) 
     */
    public void insertClient(String id, String name,
                                  String contactNo, String mail) throws MyException {
        DatabaseConnectivity databaseConnectivity;
        Connection connection;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ClientDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(
                                      "INSERT INTO " + CLIENT_TABLE + "(" 
                                                  + CLIENT_ID+"," 
                                                  + CLIENT_NAME + "," 
                                                  + CLIENT_MAIL + ","
                                                  + CLIENT_CONTACT_NO 
                                                  + ") VALUES (?,?,?,?)");
            statement.setString(1, id);
            statement.setString(2, name);
            statement.setString(3, mail);
            statement.setString(4, contactNo);
            statement.executeUpdate();
            statement.close();                          
        } catch(SQLException sqlException) {
            LogManager.debug(ClientDaoImpl.class, "Sql exception while "
                                    + "inserting client :" + id, sqlException);
            throw new MyException("Can't add client! Try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ClientDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }
    }
    
    /**
     * @see com.ideas2it.dao.ClientDao  #public Client getClientById(
     *                                                               String id)
     */
    public Client getClientById(String id) throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ClientDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(
                                                 "SELECT "
                                                  + CLIENT_ID+"," 
                                                  + CLIENT_NAME + "," 
                                                  + CLIENT_MAIL + ","
                                                  + CLIENT_CONTACT_NO 
                                              + " FROM " + CLIENT_TABLE 
                                              + " WHERE " + CLIENT_ID+ "= ?");
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery(); 
            if (resultSet.next()) {
                Client client = new Client(
                                    resultSet.getString(CLIENT_ID),
                                    resultSet.getString(CLIENT_NAME),
                                    resultSet.getString(CLIENT_CONTACT_NO),
                                    resultSet.getString(CLIENT_MAIL));                                         
                resultSet.close();
                statement.close();
                return client;                              
            }                            
        } catch(SQLException sqlException) {
            LogManager.debug(ClientDaoImpl.class, "SQL exception while "
                                + "fetching detail of id :" + id, sqlException);
            throw new MyException("Can't get the details!try again");      
        }finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ClientDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }      
        return null;
    }
    
    /**
     * @see com.ideas2it.dao.ClientDao  #public void removeClientById(
     *                                                                String id)
     */
    public void removeClientById(String id) throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ClientDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            
            PreparedStatement statement = connection.prepareStatement(
                                           "DELETE FROM " +"ADDRESS_BOOK" 
                                             + " WHERE "+ CLIENT_ID+" = ?");
            statement.setString(1, id);
            statement = connection.prepareStatement("SELECT " + PROJECT_ID +  
                              " FROM " +  PROJECT_TABLE + " WHERE " 
                                 + CLIENT_ID
                                 + " = ?");
            ResultSet resultSet = statement.executeQuery();
            statement = connection.prepareStatement("UPDATE "  
                                 + EMPLOYEE_TABLE + " SET " 
                                 + PROJECT_ID
                                 + " = null WHERE "
                                 + PROJECT_ID + "= ?");
            while (resultSet.next()) {
                statement.setString(1, resultSet.getString(PROJECT_ID));
                statement.executeUpdate();
            }         
            resultSet.close();           
            statement = connection.prepareStatement("UPDATE "  
                                 +  EMPLOYEE_TABLE + " SET " 
                                 + PROJECT_ID
                                 + " = null WHERE "
                                 + PROJECT_ID + "= ?");
            statement = connection.prepareStatement("DELETE FROM " 
                               + PROJECT_TABLE + " WHERE " + CLIENT_ID 
                               + " = ?");
            statement.setString(1, id); 
            statement.executeUpdate();
            statement = connection.prepareStatement("DELETE FROM " 
                                  + CLIENT_TABLE + " WHERE "+ CLIENT_ID+" = ?");             
            statement.setString(1, id);
            statement.executeUpdate();    
            statement.close();                  
        } catch(SQLException sqlException) {
            LogManager.debug(ClientDaoImpl.class, "Exception while deleting "
                                                + "client :" +id, sqlException);
            throw new MyException("Can't remove the client!Try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ClientDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }      
    } 
    
    /**
     * @see com.ideas2it.dao.ClientDao  # public void updateClientDetail(
     *                                      String columnName, String newDetail,
     *                                                      String clientId)
     */
    public void updateClientDetail(String columnName, String newDetail,
                                     String clientId) throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ClientDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(
                                      "UPDATE " + CLIENT_TABLE + " SET "
                                                   + columnName + " = ? WHERE "
                                                       + CLIENT_ID+ " = ?");   
            statement.setString(1,newDetail);
            statement.setString(2, clientId);
            statement.executeUpdate();
            statement.close(); 
        } catch(SQLException sqlException) {
            LogManager.debug(ClientDaoImpl.class, "SQL exception thrown while "
                                              + "updating column " + columnName 
                                              + "of PROJECT table where "
                                              + "client id :" + clientId,
                                              sqlException);
            throw new MyException("Can't update the client!Try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ClientDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }  
    }
    
    /**
     * @see com.ideas2it.dao.ClientDao; 
     *                                  #public List<Client> getAllClients()
     */
    public List<Client> getAllClients() throws MyException {        
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ClientDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            Statement statement = connection.createStatement();
            List<Client> clients = new ArrayList<Client>();
            Client client = null;        
            ResultSet resultSet = statement.executeQuery("SELECT "
                                              + CLIENT_ID+"," 
                                              + CLIENT_NAME + "," 
                                              + CLIENT_MAIL + ","
                                              + CLIENT_CONTACT_NO 
                                              + " FROM " + CLIENT_TABLE);
            while (resultSet.next()) {
                client = new Client(resultSet.getString(CLIENT_ID),
                                        resultSet.getString(CLIENT_NAME),
                                      resultSet.getString(CLIENT_CONTACT_NO),
                                        resultSet.getString(CLIENT_MAIL));
                clients.add(client);
            }
            resultSet.close();
            statement.close(); 
            return clients;
        } catch(SQLException sqlException) {
            LogManager.debug(ClientDaoImpl.class, "Exception while "
                                            + "fetching data from table :" 
                                            + CLIENT_TABLE, sqlException);
            throw new MyException();
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ClientDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }    
    }
}
