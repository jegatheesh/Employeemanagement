package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ideas2it.database.DatabaseConnectivity;
import com.ideas2it.exception.MyException;
import com.ideas2it.logs.LogManager;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.dao.ProjectDao;


/**
 * Inserts , updates, deletes the entrys in the project table
 */
public class ProjectDaoImpl implements ProjectDao {
    private static final String PROJECT_TABLE_NAME = "PROJECT";
    private static final String EMPLOYEE_TABLE_NAME = "EMPLOYEE";
    private static final String EMPLOYEE_ID = "EMPLOYEE_ID";
    private static final String EMPLOYEE_NAME = "NAME";
    private static final String EMPLOYEE_MAIL = "MAIL";
    private static final String EMPLOYEE_DESIGNATION = "DESIGNATION";
    private static final String EMPLOYEE_DOB = "DOB";
    private static final String PROJECT_ID= "PROJECT_ID";
    private static final String PROJECT_NAME = "PROJECT_NAME";
    private static final String CLIENT_ID = "CLIENT_ID";
    private static final String PROJECT_DOMAIN = "PROJECT_DOMAIN";
   
    /**
     * @see com.ideas2it.dao.ProjectDao  #insertProject(String id, 
     *                                             String name, String clientId)
     */
    public void insertProject(String id, String name, String clientId,
                                      String projectDomain) throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity=DatabaseConnectivity.getInstance();                                                          
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(
                                       "INSERT INTO " + PROJECT_TABLE_NAME + "(" 
                                       + PROJECT_ID+"," + PROJECT_NAME + "," 
                                       + CLIENT_ID + PROJECT_DOMAIN
                                       + ") VALUES (?,?,?,?)");
            statement.setString(1, id);
            statement.setString(2, name);
            statement.setString(3, clientId);
            statement.setString(4, projectDomain);
            statement.executeUpdate();  
            statement.close();                           
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Sql exception while "
                                    + "inserting employee " + id, sqlException);
            throw new MyException("Can't add project! Try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                                     + "closing the connection", sqlException);           
            }
            
        }  
    }
    /**
     *@see com.ideas2it.dao.ProjectDao # getProjectById(String id)
     */
    public Project getProjectById(String id) throws MyException {
        DatabaseConnectivity databaseConnectivity;
        Connection connection;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
            }
        try {     
            PreparedStatement statement = connection.prepareStatement("SELECT "                                                       
                                         + PROJECT_ID + "," + PROJECT_NAME + "," 
                                         + CLIENT_ID + PROJECT_DOMAIN
                                         + " FROM " + PROJECT_TABLE_NAME
                                         + " WHERE " + PROJECT_ID+ "= ?");
            statement.setString(1,id);
            ResultSet resultSet = statement.executeQuery(); 
            if (resultSet.next()) {
                Project project = new Project(resultSet.getString(PROJECT_ID),
                                            resultSet.getString(PROJECT_NAME),
                                            resultSet.getString(CLIENT_ID),
                                           resultSet.getString(PROJECT_DOMAIN)); 
                resultSet.close();
                statement.close();
                return project;                               
            }                                     
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "SQL exception while "
                                + "fetching detail of id :" + id, sqlException);
            throw new MyException("Can't get the details!try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }
        return null;
    }
    
    /**
     * @see com.ideas2it.dao.ProjectDao   # removeProjectById(
     *                                                              String id)
     */
    public void removeProjectById(String id) throws MyException {
        DatabaseConnectivity databaseConnectivity;
        Connection connection;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {     
            PreparedStatement statement = connection.prepareStatement("UPDATE "  
                                               +  EMPLOYEE_TABLE_NAME + " SET " 
                                               + PROJECT_ID + " = null WHERE "
                                               + PROJECT_ID+ "= ?");
            statement.setString(1, id);
            statement.executeUpdate();
            statement = connection.prepareStatement("DELETE FROM " 
                                             + PROJECT_TABLE_NAME + " WHERE "
                                             + PROJECT_ID+" = ?");
            statement.setString(1, id);
            statement.executeUpdate();
            statement.close();                 
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "SQL exception thrown while "
                                + "deleting columns" + PROJECT_ID + " of " 
                                + PROJECT_TABLE_NAME + " of employee id :" + id,
                                                                 sqlException);
            throw new MyException("Can't remove the project!Try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }      
    }
    /**
     * @see com.ideas2it.dao.ProjectDao  # updateProjectDetail(
     *                                     String columnName, String newDetail, 
     *                                                               String id)
     */
    public void updateProjectDetail(String columnName , String newDetail, 
                                               String id) throws MyException {
        
        DatabaseConnectivity databaseConnectivity;
        Connection connection;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {   
            PreparedStatement statement = connection.prepareStatement("UPDATE "                                                          
                                                  + PROJECT_TABLE_NAME + " SET "
                                                    + columnName + " = ? WHERE "
                                                       + PROJECT_ID+ " = ?");
            statement.setString(1, newDetail);
            statement.setString(2, id);
            statement.executeUpdate(); 
            statement.close();                  
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "SQL exception thrown while "
                                              + "updating column " + columnName 
                                              + "of PROJECT table where "
                                            + "project id " + id, sqlException);
            throw new MyException("Can't update the project!Try again");
        } finally { 
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                                      + "closing the connection", sqlException);             
            }
        }      
    }
    
    /**
     * @see com.ideas2it.dao.ProjectDao #getAllProjects()
     */
    public List<Project> getAllProjects() throws MyException {
        DatabaseConnectivity databaseConnectivity;
        Connection connection;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {     
            Statement statement = connection.createStatement();
            List<Project> projects = new ArrayList<Project>();
            Project project = null;
            ResultSet resultSet = statement.executeQuery("SELECT "
                                              + PROJECT_ID+"," 
                                              + PROJECT_NAME + "," 
                                              + CLIENT_ID 
                                              + " FROM " + PROJECT_TABLE_NAME);
            while (resultSet.next()) {
                project = new Project(resultSet.getString(PROJECT_ID),
                                        resultSet.getString(PROJECT_NAME),
                                        resultSet.getString(CLIENT_ID),
                                        resultSet.getString(PROJECT_DOMAIN));
                projects.add(project);
            }   
            resultSet.close();
            statement.close();              
            return projects;
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Sql exception while "
                                            + "fetching data from table :" 
                                            + PROJECT_TABLE_NAME, sqlException);
            throw new MyException();
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }      
    }
    
    /**
     * @see com.ideas2it.dao.ProjectDao #getEmployeeProjectId(
     *                                                       String employeeId)
     */
     public String getEmployeeProjectId(String employeeId) throws 
                                                           MyException {
        DatabaseConnectivity databaseConnectivity;
        Connection connection;
        ResultSet resultSet = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {     
            PreparedStatement statement = connection.prepareStatement("SELECT "
                                                + PROJECT_ID + " FROM " 
                                                + EMPLOYEE_TABLE_NAME +" WHERE "
                                                + EMPLOYEE_ID +" = ?");
            statement.setString(1, employeeId);
            resultSet = statement.executeQuery();     
            if (resultSet.next()) {          
                return resultSet.getString(PROJECT_ID);                
            }                    
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't fetch employee "
                                            + "details! Database connection"
                                            + " down! Try again", sqlException);
        } finally {
            try {
                resultSet.close();
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                                    + "closing the connection ", sqlException);            
            }
        }      
        return null; 
    }
    
    /**
     *@see com.ideas2it.dao.ProjectDao  #assignEmployee(
     *                                      String employeeId, String projectId)
     */
    public void assignEmployee(String employeeId, String projectId) throws 
                                                           MyException {
        DatabaseConnectivity databaseConnectivity;
        Connection connection;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try { 
            PreparedStatement statement = connection.prepareStatement("UPDATE "
                                        + EMPLOYEE_TABLE_NAME + " SET " 
                                        + PROJECT_ID+"= ? WHERE " + EMPLOYEE_ID 
                                        + "=?");
            statement.setString(1, projectId);
            statement.setString(2, employeeId);
            statement.executeUpdate();  
            statement.close();                                   
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't access database! "
                     + "Database connection down! Try again", sqlException);
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }       
    }
    /**
     * @see com.ideas2it.dao.ProjectDao #removeEmployeeFromProject(
     *                                                      String employeeId)
     */
    public void removeEmployeeFromProject(String employeeId) throws 
                                                           MyException {
        DatabaseConnectivity databaseConnectivity;
        Connection connection;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't establish connection "
                                                 + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {     
            PreparedStatement statement = connection.prepareStatement("UPDATE "                                                      
                                 +  EMPLOYEE_TABLE_NAME + " SET " + PROJECT_ID
                                 + " = null WHERE " + PROJECT_ID+ "= ?");
            statement.setString(1, employeeId);
            statement.executeUpdate();
            statement.close();                                             
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                                     + "updating employee:", sqlException );
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);          
            }
        }       
    }
    /**
     *@see com.ideas2it.dao.ProjectDao 
     *                             #getAllEmployeesByProjectId(
     *                                                        String projectId)
     */
    public List<Employee> getAllEmployeesByProjectId(String projectId) throws
                                                           MyException {  
        DatabaseConnectivity databaseConnectivity ;
        Connection connection; 
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Can't establish connection "
                                                 + "to the DB", sqlException);
            throw new MyException("Can't connect!check connection");
        }
        try {  
            PreparedStatement statement = connection.prepareStatement("SELECT "                                         
                             + EMPLOYEE_ID + "," + EMPLOYEE_NAME + ","
                             + EMPLOYEE_MAIL + "," + EMPLOYEE_DESIGNATION + ","
                             + EMPLOYEE_DOB + "," + PROJECT_ID + " FROM " 
                             + EMPLOYEE_TABLE_NAME + " WHERE "+ PROJECT_ID
                             + "= ?");
            statement.setString(1, projectId);
            ResultSet resultSet = statement.executeQuery();
            resultSet = statement.executeQuery();
            List<Employee> employees = new ArrayList<Employee>(); 
            while(resultSet.next()) {
                Employee employee = new Employee(
                                    resultSet.getString("EMPLOYEE_ID"),
                                    resultSet.getString("NAME"),
                                    resultSet.getString("DESIGNATION"),
                                    resultSet.getString("MAIL"),
                                    resultSet.getString("DOB"));
                employee.setProjectId(resultSet.getString("PROJECT_ID"));
                employees.add(employee);   
            }          
            resultSet.close(); 
            statement.close();                
            return employees;              
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                 + "fetching data from " + EMPLOYEE_ID + "," + EMPLOYEE_NAME 
                 + "," + EMPLOYEE_MAIL + "," + EMPLOYEE_DESIGNATION + "," 
                 + EMPLOYEE_DOB + "," + PROJECT_ID + " FROM " 
                 + EMPLOYEE_TABLE_NAME, sqlException);
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(ProjectDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        return null;
        }        
    }  
}
