package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import com.ideas2it.dao.AddressDao;
import com.ideas2it.database.DatabaseConnectivity;
import com.ideas2it.logs.LogManager;
import com.ideas2it.model.Address;
import com.ideas2it.exception.MyException;

public class AddressDaoImpl implements AddressDao {
    private static final String ADDRESS_TABLE = "ADDRESS_BOOK";
    private static final String EMPLOYEE_ID = "EMPLOYEE_ID";
    private static final String CLIENT_ID = "CLIENT_ID";
    private static final String DOOR_NO = "DOOR_NO";
    private static final String LAND_MARK = "LAND_MARK";
    private static final String STREET = "STREET";
    private static final String CITY = "CITY";
    private static final String DISTRICT = "DISTRICT";
    private static final String STATE = "STATE";
    private static final String COUNTRY = "COUNTRY";
    private static final String POSTCODE = "POSTCODE";
    private static final String ADDRESS_TYPE = "ADDRESS_TYPE";
    /**
     * @see com.ideas2it.dao.AddressDao  #
     */
    public void insertAddress(String id,String addressType, String doorNo,
                    String landMark,String street, String city, String district,
                              String state, String country, String postCode)
                                                           throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();     
        } catch (SQLException sqlException) {
            LogManager.debug(AddressDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        } 
        try {
            PreparedStatement statement;
            String[] splittedString = id.split("-");
            if (splittedString[0].equals("C")) {
                statement = connection.prepareStatement("INSERT INTO "
                         + ADDRESS_TABLE + "(" + CLIENT_ID +","+ DOOR_NO +","
                         + LAND_MARK +","+ STREET +","+ CITY +","+ DISTRICT +","
                     + STATE +","+ COUNTRY +","+ POSTCODE + "," + ADDRESS_TYPE +
                         ") VALUES(?,?,?,?,?,?,?,?,?,?)" );
            } else {
                statement = connection.prepareStatement("INSERT INTO "
                         + ADDRESS_TABLE + "(" + EMPLOYEE_ID +","+ DOOR_NO +","
                        + LAND_MARK +","+ STREET +","+ CITY +","+ DISTRICT +","
                       + STATE +","+ COUNTRY +","+ POSTCODE  + ","+ ADDRESS_TYPE 
                         +") VALUES(?,?,?,?,?,?,?,?,?,?)" );
            }
            statement.setString(1, id);
            statement.setString(2, doorNo);
            statement.setString(3, landMark);
            statement.setString(4, street);
            statement.setString(5, city);
            statement.setString(6, district);
            statement.setString(7, state);
            statement.setString(8, country);
            statement.setString(9, postCode);
            statement.setString(10, addressType);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException sqlException) {
            LogManager.debug(ProjectDaoImpl.class, "Sql exception while "
                             + "inserting address for id :" + id, sqlException);
            throw new MyException("Can't add address! Try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(AddressDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }
    }
    
    public void removeAddressById(String id) {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();     
        } catch (SQLException sqlException) {
            LogManager.debug(AddressDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            PreparedStatement statement;
            String[] splittedString = id.split("-");
            if (splittedString.equals("C")) {
                statement = connection.prepareStatement(
                                               "DELETE FROM " + ADDRESS_TABLE   
                                                 + " WHERE "+ CLIENT_ID+" = ?");
            } else {
                statement = connection.prepareStatement(
                                               "DELETE FROM " + ADDRESS_TABLE   
                                               + " WHERE "+ EMPLOYEE_ID+" = ?");
            }
            statement.setString(1, id);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "Exception while deleting "
                                        + "address for id :" +id, sqlException);
            throw new MyException("Can't delete the address!Try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(AddressDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }      
    }
    
    public void updateAddressById(String id, String addressType, String doorNo, 
                   String landMark, String street, String city, String district,
                              String state, String country, String postCode)
                                                           throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();     
        } catch (SQLException sqlException) {
            LogManager.debug(AddressDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        } 
        try {
            PreparedStatement statement;
            String[] splittedString = id.split("-");
            if (splittedString.equals("C")) {
                statement = connection.prepareStatement("UPDATE "
                   + ADDRESS_TABLE + " SET " + DOOR_NO +"=?,"+ LAND_MARK +"=?,"
                   + STREET +"=?,"+CITY +"=?,"+ DISTRICT +"=?,"+ STATE +"=?,"
                   + COUNTRY +"=?,"+ POSTCODE +  "=? WHERE " + CLIENT_ID 
                   + "= ? && " + ADDRESS_TYPE + " = ?" );
            } else {
                statement = connection.prepareStatement("UPDATE "
                    + ADDRESS_TABLE + " SET " + DOOR_NO +"=?,"+ LAND_MARK +"=?,"
                    + STREET +"=?,"+ CITY +"=?,"+ DISTRICT +"=?,"+ STATE +"=?,"
                    + COUNTRY +"=?,"+ POSTCODE + "=? WHERE " + EMPLOYEE_ID 
                    + "= ? && " + ADDRESS_TYPE + " = ?" );
            
            }
            statement.setString(1, doorNo);
            statement.setString(2, landMark);
            statement.setString(3, street);
            statement.setString(4, city);
            statement.setString(5, district);
            statement.setString(6, state);
            statement.setString(7, country);
            statement.setString(8, postCode);
            statement.setString(9, id);
            statement.setString(10, addressType);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException sqlException) {
            LogManager.debug(EmployeeDaoImpl.class, "SQL exception thrown while "
                                             + "updating address for id :" + id,
                                              sqlException);
            throw new MyException("Can't update the employee!Try again");
        } finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(AddressDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }
    }
    
    public List<Address> getAllAddresses() throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(AddressDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            List <Address> addresses = new ArrayList<Address>();
            PreparedStatement statement = connection.prepareStatement(                                               
                "SELECT " + EMPLOYEE_ID + "," + CLIENT_ID + "," + DOOR_NO + ","
                + LAND_MARK + "," + STREET + "," + CITY + "," + DISTRICT + ","  
                + STATE + "," + COUNTRY + "," + POSTCODE + "," + ADDRESS_TYPE  
                                                  + " FROM " + ADDRESS_TABLE );
            ResultSet resultSet = statement.executeQuery(); 
            while (resultSet.next()) {
                String ID ;
                if (null == resultSet.getString(EMPLOYEE_ID)) {
                    ID = CLIENT_ID;
                } else {
                    ID = EMPLOYEE_ID;
                }
                Address address = new Address(
                                    resultSet.getString(ID),
                                    resultSet.getString(ADDRESS_TYPE),
                                    resultSet.getString(DOOR_NO),
                                    resultSet.getString(LAND_MARK),
                                    resultSet.getString(STREET),
                                    resultSet.getString(CITY),
                                    resultSet.getString(DISTRICT),
                                    resultSet.getString(STATE),
                                    resultSet.getString(COUNTRY),
                                    resultSet.getString(POSTCODE));                                                                            
                addresses.add(address);                              
            }
            resultSet.close();
            statement.close();           
            return addresses;              
        } catch(SQLException sqlException) {
            LogManager.debug(AddressDaoImpl.class, "Sql exception while "
                                          + "getting all the addresses to id :", 
                                                                sqlException);
            throw new MyException("Can't add address! Try again");
        }finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(AddressDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }      
    }
    
    public List<Address> getAddressById(String id) throws MyException {
        DatabaseConnectivity databaseConnectivity = null;
        Connection connection = null;
        try {
            databaseConnectivity = DatabaseConnectivity.getInstance();
            connection = databaseConnectivity.connectToDataBase();
        } catch (SQLException sqlException) {
            LogManager.debug(AddressDaoImpl.class, "Can't establish connection "
                                                   + "to the DB", sqlException);
            throw new MyException("Can,t connect!check connection");
        }
        try {
            List <Address> addresses = new ArrayList<Address>();
            PreparedStatement statement;
            String[] splittedString = id.split("-");
            if (splittedString.equals("C")) {
                statement = connection.prepareStatement(                                               
                          "SELECT " + DOOR_NO + "," + LAND_MARK + "," + STREET 
                          + "," + CITY + "," + DISTRICT + "," + STATE + ","                          
                          + COUNTRY + "," + POSTCODE + "," + ADDRESS_TYPE + "," 
             + "," + " FROM " + ADDRESS_TABLE  + " WHERE " + CLIENT_ID + "= ?");
            } else {
                statement = connection.prepareStatement(                                               
                          "SELECT " + DOOR_NO + "," + LAND_MARK + "," + STREET 
                           + "," + CITY + "," + DISTRICT + "," + STATE + ","                     
                              + COUNTRY + "," + POSTCODE + "," + ADDRESS_TYPE  
                  + " FROM " + ADDRESS_TABLE + " WHERE " + EMPLOYEE_ID + "= ?");
            }
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery(); 
            while (resultSet.next()) {
                Address address = new Address(
                                    id,
                                    resultSet.getString(ADDRESS_TYPE),
                                    resultSet.getString(DOOR_NO),
                                    resultSet.getString(LAND_MARK),
                                    resultSet.getString(STREET),
                                    resultSet.getString(CITY),
                                    resultSet.getString(DISTRICT),
                                    resultSet.getString(STATE),
                                    resultSet.getString(COUNTRY),
                                    resultSet.getString(POSTCODE));                                                                            
                addresses.add(address);                              
            }        
            resultSet.close();
            statement.close();   
            return addresses;                 
        } catch(SQLException sqlException) {
            LogManager.debug(AddressDaoImpl.class, "SQL exception while "
                                      + "fetching address detail of id :" + id,
                                                                 sqlException);
            throw new MyException("Can't get the address!try again");
        }finally {
            try {
                databaseConnectivity.closeConnection();
            } catch(SQLException sqlException) {
                LogManager.debug(AddressDaoImpl.class, "SQl exception while "
                                     + "closing the connection ", sqlException);            
            }
        }      
    }
}
