package com.ideas2it.dao;

import java.util.List;

import com.ideas2it.exception.MyException;
import com.ideas2it.model.Client;

/**
 * Inserts , updates, deletes the entrys in the client table
 */
public interface ClientDao {
    
    public void insertClient(String id, String name,
                                  String contactNo, String mail) throws MyException;
        
    /**
     * @see com.ideas2it.dao.ClientDao  #public Client getClientById(
     *                                                               String id)
     */
    public Client getClientById(String id) throws MyException;
        
    
    /**
     * @see com.ideas2it.dao.ClientDao  #public void removeClientById(
     *                                                                String id)
     */
    public void removeClientById(String id) throws MyException;
        
    
    /**
     * @see com.ideas2it.dao.ClientDao  # public void updateClientDetail(
     *                                      String columnName, String newDetail,
     *                                                      String clientId)
     */
    public void updateClientDetail(String columnName, String newDetail,
                                     String clientId) throws MyException;
        
    
    /**
     * @see com.ideas2it.dao.ClientDao; 
     *                                  #public List<Client> getAllClients()
     */
    public List<Client> getAllClients() throws MyException;        
        
}
