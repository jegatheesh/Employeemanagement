package com.ideas2it.dao;

import java.util.List;

import com.ideas2it.exception.MyException;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;

/**
 * Inserts , updates, deletes the entrys in the project table
 */
public interface ProjectDao {
     
    /**
     * <p>
     * Creates new project that holds the details id, name, client
     * </p> 
     * @param    id          project id, a unique entity for identification 
     * @param    name        name of the project not necessary to be unique
     * @param    client      project client not necessarily to be unique
     * @return   returns true if created the project else false
     */
    public void insertProject(String id, String name, String clientId,
                              String projectDomain) throws MyException;
    
    /**
     * <p>
     * Gets the particular project by id
     * </p>
     * @param     id     project id, a unique entity for identification
     * @return    project details to if the project id in parameter
     */
    public Project getProjectById(String id) throws MyException;
    /**
     * <p>
     * Removes the particular project by the id
     * <p/>
     * @param      id    project id, a unique entity for identification
     */
    public void removeProjectById(String id) throws MyException;
    /**
     * <p>
     * Updates the particular project detail provided the column name and 
     * the project id 
     * </p>
     * @param    column       column name that holds the detail to be altered
     * @param    newDetail    newDetail to be replaced
     * @param    id           id of project ,an unique entity for identification
     */
    public void updateProjectDetail(String columnName , String newDetail, String id)
                                                    throws MyException;
    
    /**
     * <p>
     * Gets all the projects' details
     * </p>
     * returns all the details of the project or null if there is no projects 
     * stored before or deleted the projects priorly 
     */
    public List<Project> getAllProjects() throws MyException;
    
    /**
     * <p>
     * Gets the employee' project id 
     * </p>
     * @param   employeeId   id of employee ,an unique entity for identification
     * @return    returns the project id of employee whose id is passed
     */
    public String getEmployeeProjectId(String employeeId) throws 
                                                           MyException;
    
    /**
     * <p>
     * Assigns employee to the project 
     * </p>
     * @param   employeeId    id of employee,an unique entity for identification
     * @param   projectId     id of project, an unique entity for identification
     */
    public void assignEmployee(String employeeId, String projectId) throws 
                                                           MyException;    
    /**
     * <p>
     * Removes an employee from project, provided the employee id 
     * by tagging the employees project id to null
     * </p>
     * @param   employeeId  id of employee ,an unique entity for identification
     */
    public void removeEmployeeFromProject(String employeeId) throws 
                                                           MyException;
    /**
     * <p>
     * Gets and returns all the employees for particular project id
     * </p>
     * @param    projectId    id of project ,an unique entity for identification
     * @return   returns all the employee name as list
     */
    public List<Employee> getAllEmployeesByProjectId(String projectId) throws
                                                           MyException;    
}
