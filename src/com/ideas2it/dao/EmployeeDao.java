package com.ideas2it.dao;

import java.util.List;

import com.ideas2it.exception.MyException;
import com.ideas2it.model.Employee;

public interface EmployeeDao {
 
    /**
     * <p>
     * Inserts new employee detail that holds the employee id, name, designation,
     * mail, dob
     * </p>
     * @param    id          id of employee ,an unique entity for identification
     * @param    name        employee name, not necessary to be unique 
     * @param    designation employee designation, not necessary to be unique 
     * @param    mail        employee mail, an unique entity for each employee 
     * @param    dob         employee dob, not necessarily unique
     */
    public void insertEmployee(String id, String name, String designation,
                               String mail, String dob) throws MyException;
 
    /**
     * <p>
     * Gets the details of particular employee detail by employee id 
     * </p>
     * @param     id      id of employee, an unique entity for identification
     * @return    employee details of the employee id or null of the employee
     *            whose id provided not exist
     */
    public Employee getEmployeeById(String id) throws MyException;
    
    /**
     * <p>
     * Deletes particular employee detail by employee id
     * </p> 
     * @param   id   id of employee, an unique entity for identification
     */
    public void removeEmployeeById(String id) throws MyException;
    
    /**
     * <p>
     * Updates the old employee details with the new details
     * </p>
     * @param     columnName    columnName indicates the name of column under 
     *                          which the detail to be modified is present
     * @param     newDetail     the new detail for updation
     * @param     employeeId    employeeId , a unique entity for identification
     *                          points the particular employee detail to update
     */  
    public void updateEmployeeDetail(String columnName, String newDetail,
                                     String employeeId) throws MyException;
   
    /**
     * <p>
     * Gets all the employee details stored
     * </p> 
     * @return    all the employee details as list of employee model object
     */      
    public List<Employee> getAllEmployees() throws MyException;
}
