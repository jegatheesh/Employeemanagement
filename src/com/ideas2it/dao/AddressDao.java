package com.ideas2it.dao;

import java.util.List;

import com.ideas2it.exception.MyException;
import com.ideas2it.model.Address;

public interface AddressDao {

    /**
     * <p>
     * Stores the address details and maps it to the id of the person
     * </p>
     * @param  id       
     *            id of the person, an unique entity for identification
     * @param  doorNo   
     *            door No can contain alphanumerals and special characters
     * @param  landmark
     *            landMark contains only characters, multiple words allowed 
     * @param  street 
     *            only the characters and numerals, multiple words allowed 
     * @param  city 
     *            city name contains only alphabets, multiple single word entity
     * @param  district 
     *            contains only alphabets , multiple word allowed
     * @param  state
     *            contains only alphabets , multiple word allowed
     * @param  country 
     *            contains only alphabets , multiple word allowed
     * @param  postcode
     *            contains only numerals , multiple words not allowed
     */  
    public void insertAddress(String id, String addressType, String doorNo,
                              String landMark, String street, String city, 
                              String district, String state, String country,
                              String postCode) throws MyException;
    /**
     * <p>
     * Removes the address for the id 
     * </p>
     *@param    id         
     *             id of the person, an unique entity for identification
     */
    public void removeAddressById(String id) throws MyException;
    
    /**
     * <P>
     * Updates the old address with the new one for the id provided
     * </p>
     * @param    id 
     *             id of the person, an unique entity for identification
     */
    public void updateAddressById(String id, String addressType, String doorNo,
                                  String landMark, String street, String city,
                                  String district, String state, String country,
                                   String postCode) throws MyException;
    
    /**
     * <p>
     * Gets the address for the provided id
     * </p>
     * @param   id   id of client/employee, an unique id for identification
     * @return  all the addresses for the given id
     */
    public List<Address> getAddressById(String id) throws MyException;
    
    /**
     * <p>
     * Gets all the addresses 
     * </p>
     * @return   returns all the addresses as list
     */
    public List<Address> getAllAddresses() throws MyException;
}
