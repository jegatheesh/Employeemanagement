package com.ideas2it.logs;

import java.io.File;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This is a Helper class that creates a logger for application logging
 */
public class LogManager {
   private static Logger logger;	
   
   /**
    * <p> 
    * Sets the Logger with the class Name appended
    * </p>
    * @param   Object   object for which logger is implemented
    */
    private static void setLogger(Object obj) {	       
        logger = Logger.getLogger(obj.getClass());	
        DOMConfigurator.configure(System.getProperty("user.dir") + "/log4j.xml");    	       	
    }	
    
    public static void debug(Object object, String message, Throwable cause) {
        setLogger(object);
        logger.debug(message, cause);
    }
    
    public static void error(Object object, String message, Throwable cause) {
        setLogger(object);
        logger.error(message, cause);
    }
    
    public static void info(Object object, String message, Throwable cause) {
        setLogger(object);
        logger.info(message, cause);
    }
}

