package com.ideas2it.model;

/**
 * Model class for project details
 */
public class Project {
    private String id ;
    private String name;
    private String clientId;
    private String projectDomain;
    
    public Project( String id, String name,
                    String clientId, String projectDomain) {
        this.id = id;
        this.name = name;
        this.clientId = clientId;   
        this.projectDomain = projectDomain;    
    }
    
    public Project() {
    }

    /**
     * <p>
     * Getters and Setters
     * </p>
     */
    public String getId() {
        return id;    
    }
        
    public void setId(String id) {
        this.id = id;
    }    
    
    public String getName() {
        return name;
    }    
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getClientId() {
        return clientId;
    } 
    
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    
    public String getProjectDomain() {
        return projectDomain;
    }
    
    public void setProjectDomain(String projectDomain) {
        this.projectDomain = projectDomain;
    }
    public String toString() {
        return "Project id           :" + id 
           + "\nProject name         :" + name
           + "\nProject domain       :" + projectDomain
           + "\nProject clientId       :" + clientId + "\n\n" ;
    }
}
