package com.ideas2it.model;

import com.ideas2it.util.DateUtil;
/**
 * a model class for employee data
 */
public class Employee {
    private String id;
    private String name;
    private String designation;
    private String mail;
    private String dob; 
    private String projectId;   
   
    public Employee( String id, String name, String designation, 
                                String mail, String dob) {
        this.id = id;
        this.name = name;
        this.designation = designation;
        this.mail = mail; 
        this.dob = dob;
    }
        
    public Employee() {}

    /**
     * <p>
     * Getters and Setters
     * </p>
     */
    public String getId() {
        return id;    
    }
    
    public void setId(String id) {
        this.id = id;
    }
        
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    } 
    
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getMail() {
        return mail;
    } 
        
    public void setMail(String mail) {
        this.mail = mail;
    }
    
    public void setDob(String dob) {
        this.dob = dob;
    }
    
    public int getAge() {
        return DateUtil.calculateAge(dob);
    }
    
    public String getDob() {
        return dob;
    }
    
    public String getProjectId() {
        return projectId;
    }
    
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String toString() {
        return "Employee id           :" + id 
           + "\nEmployee name         :" + name
           + "\nEmployee designation  :" + designation
           + "\nEmployee mail         :" + mail
           + "\nEmployee dob          :" + dob 
           + "\nEmployee age          :" + getAge()
           + "\nEmployee project id   :" + projectId;
    }
}
