package com.ideas2it.model;

/**
 * <p>
 * Model class for address has the fields doorNo, landmark, street, city, 
 * district, state, country, postcode 
 * </p>
 */
public class Address {
    private String id;
    private String addressType;
    private String doorNo;
    private String landMark;
    private String street;
    private String city;
    private String district;
    private String state;
    private String country;
    private String postcode;
    
    public Address(String id, String addressType, String doorNo, String landMark,
        String street,String city,String district, String state, String country,
                                                              String postcode) {
        this.id = id;
        this.addressType = addressType;
        this.doorNo = doorNo;
        this.street = street;
        this.landMark = landMark;
        this.district = district;
        this.city = city;
        this.state = state;
        this.country = country;
        this.postcode = postcode;
    }
    
    /**
     * <p>
     * Getters and setters 
     * </p>
     */
     
    public String getId() {
        return this.id;
    }
    
    public String getAddressType () {
        return this.addressType;
    }
    
    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }
    
    public String getDoorNo() {
        return this.doorNo;
    }

    public String getStreet() {
        return this.street;
    }
    
    public String getlandMark() {
        return this.landMark;
    }

    public String getDistrict() {
        return this.district;
    }

    public String getCity() {
        return this.city;
    }
    
    public String getState() {
        return this.state;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public String getPostcode() {
        return this.postcode;
    }

    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }

    public void setStreet(String street) {
        this.street = street;
    }
    
    public void setlandMark(String landMark) {
        this.landMark = landMark;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    public void setState(String state) {
        this.state = state;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    
    public void setPostcode(String postCode) {
        this.postcode = postCode;
    }

    public String toString() {       
        return addressType + ":\n" + doorNo + ","
                                         + street + ", "
                                         + landMark + ",\n"
                                         + city + ","
                                         + district + "(Dt),\n"
                                         + state + ","
                                         + country + "-"
                                         + postcode + "\n";
    }
}
