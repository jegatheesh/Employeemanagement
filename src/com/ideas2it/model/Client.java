package com.ideas2it.model;

import com.ideas2it.util.DateUtil;
/**
 * a model class for client data
 */
public class Client {
    private String id;
    private String name;
    private String contactNo;
    private String mail;  
   
    public Client( String id, String name, String contactNo, String mail) {
        this.id = id;
        this.name = name;
        this.contactNo = contactNo;
        this.mail = mail; 
    }
    
    /**
     * <p>
     * Getters and setters
     * </p> 
     */  

    public String getId() {
        return id;    
    }   
    
    public void setId(String id) {
        this.id = id;
    }
        
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getContactNo() {
        return contactNo;
    } 
    
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }
 
    public String getMail() {
        return mail;
    } 
        
    public void setMail(String mail) {
        this.mail = mail;
    }

    public String toString() {
        return "Client id           :" + id 
           + "\nClient name         :" + name
           + "\nClient contactNo    :" + contactNo
           + "\nClient mail         :" + mail
           + "\n";
    }
}
