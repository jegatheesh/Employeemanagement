package com.ideas2it.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * <p>
 * Utility class for doing operation on dates
 * </p>
 */
public class DateUtil {
   
    /**
     * <p>
     * Calculates the age for given dob
     * </p>
     * @param     dob    dob is in String in the format DD-MM-YYYY
     * @return    returns the age for the given dob
     */
    public static int calculateAge(String dob)  {
        String[] splittedDateField = dob.split("-");
        int year  = Integer.parseInt(splittedDateField[2]);
        int month = Integer.parseInt(splittedDateField[1]);
        int day   = Integer.parseInt(splittedDateField[0]);
        Calendar thisInstant = new GregorianCalendar();
        int age = thisInstant.get(Calendar.YEAR) - year;
        if ((month > thisInstant.get(Calendar.MONTH)
               || (thisInstant.get(Calendar.MONTH) == month)
               && day > thisInstant.get(Calendar.DAY_OF_MONTH))) {
            age--;
        }
        return age;
     } 
}
