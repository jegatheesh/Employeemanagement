package com.ideas2it.util;


import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.regex.Pattern;

/**
 * <p>
 * Provides methods for validation of various data types
 * </p>
 */
public class CommonUtil {

    /**
     * <p>
     * Validates the mail
     * </p>
     * @param  mail  mail in string type
     * @return returns true if the input matches else false
     */
    public static boolean isMailValid(String mail) {
        return Pattern.matches("[a-z_!#$%&'\\*\\+-/=\\?^_`{|}~;]*(@)[a-z]*" +
                                       "\\.((com)||((co\\.)[a-z]{2}))",mail);
    }
    
    /**
     * <p>
     * Validates the name
     * </p>
     * @param  name  name in string type
     * @return returns true if the input matches else false
     */
    public static boolean isNameValid(String name) {
        return Pattern.matches("[A-Za-z]*", name);
    }
    
    /**
     * <p>
     * Validates the designation
     * </p>
     * @param  designation  designation in string type
     * @return returns true if the input matches else false
     */
    public static boolean isDesignationValid(String designation) {
        return Pattern.matches("[A-Za-z]*",designation);
    }
    
    /**
     * <p>
     * Validates the id
     * </p>
     * @param  id  id in string type
     * @return returns true if the input matches else false
     */
    public static boolean isIdValid(String id) {
        return Pattern.matches("[PC]\\-[0-9]{2}",id);
    }
    
    /**
     * <p>
     * Validates the Client id
     * </p>
     * @param  id  id in string type
     * @return returns true if the input matches else false
     */
    public static boolean isClientIdValid(String id) {
        return Pattern.matches("[C]\\-[0-9]{2}", id);
    }
    
    /**
     * <p>
     * Validates the employee id
     * </p>
     * @param  id  id in string type
     * @return returns true if the input matches else false
     */
    public static boolean isEmployeeIdValid(String id) {
        return Pattern.matches("[E]\\-[0-9]{2}", id);
    }
    
    /**
     * <p>
     * Validates the project id
     * </p>
     * @param  id  id in string type
     * @return returns true if the input matches else false
     */
    public static boolean isProjectIdValid(String id) {
        return Pattern.matches("[P]\\-[0-9]{2}", id);
    }
    
    /**
     * <p>
     * Validates the contact number
     * </p>
     * @param  contactNo  contact no. in string type
     * @return returns true if the input matches else false
     */
    public static boolean isValidContactNo(String contactNo) {
        return Pattern.matches("[0-9]{10}", contactNo);
    }
    
    /**
     * <p>
     * Validates the dob
     * </p>
     * @param  dob  dob in string type
     * @return returns true if the input matches else false
     */
    public static boolean isDobValid(String dob) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            df.setLenient(false);
            df.parse(dob);
            return true;
        } catch(ParseException e) {
            return false;            
        }
    }      
}
