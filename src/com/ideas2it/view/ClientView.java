package com.ideas2it.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.ideas2it.common.ConstantString;
import com.ideas2it.controller.ClientController;
import com.ideas2it.exception.MyException;
import com.ideas2it.model.Client;


/**
 * <p>
 * Makes following operations on client detail    
 * Creates new, delete an client, updates an client detail, displays 
 * particular client details, displays all the client details   
 * </p>
 * author Jegatheesh Palanisamy
 * 26/07/2017
 */
public class ClientView {
    private ClientController clientController = new ClientController();
    private Scanner input = new Scanner(System.in);
  
    /**
     * <p>
     * Displays options for add,remove,update,quit,display an client to choose
     * Does the operation based on user input 
     * </p>
     */
    public void manageClient() {    
        ClientView clientView = new ClientView();
        boolean loopExitKey = true;
        while (loopExitKey) {
            String id;
            System.out.println(ConstantString.CLIENT_OPTIONS);        
            switch(input.next()) {
                case "1" :
                    clientView.createClient();                                                             
                    break;
                case "2" : 
                    clientView.removeClientById();
                    break;
                case "3" :  
                    clientView.displayClientById();
                    break;
                case "4" :
                    clientView.displayClients();                    
                    break;
                case "5" :
                    clientView.updateClientById();
                    break;
                case "6" :
                    loopExitKey = false;
                    break;
                default:
                    break;
            }    
        }   
    }

    /** 
     * <p>
     * Gets name, id, contactNo, projectId and mail of client from user 
     * Creates the new client  
     * </p>                                             
     */
    private void createClient() throws MyException{ 
        String[] client = getValidClientDetails();
        try {
            clientController.createClient(client[0], client[1], client[2],
                                                     client[3]);
            addAddressToClient(client[0]);
            System.out.println(ConstantString.CLIENT_CREATION + 
            "\nWant to create project for this employee yes = press 'y'");
            if (input.next().equals("y")) {
                while (true) {
                    createProject(client[0]);
                    System.out.println("Want to create another" 
                                 +" project for the client yes = press 'y'");
                    if (!input.next().equals("y")) {
                        break;
                    }
                }
            }
        } catch (MyException myException) { 
            System.out.println(myException.getMessage());
        }
    }
    
    /** 
     * <p>
     * Gets the name, id, designation, dob and mail of project from user as string
     * Displays the invalid details if details do not match the pattern   
     * </p>                                              
     */
    private void createProject(String clientId) throws MyException {
        System.out.println(ConstantString.PROJECT_ID_INPUT); 
        String id = input.next();            
        System.out.println(ConstantString.PROJECT_NAME_INPUT);      
        String name = input.next();        
        System.out.println(ConstantString.PROJECT_CLIENT_INPUT);
        String projectDomain = input.next();
        clientController.createProject(id, name, projectDomain, clientId);
        System.out.println(ConstantString.PROJECT_CREATION); 
    }
    
    /**
     * <p>
     * Gets the validated employee details 
     * </p>
     * @return array of employee details in order
     */
    private String[] getValidClientDetails() {
        String[] client  = new String[4];
        int[] validFields = {1,1,1,1} ; 
        int validFieldsCount;
        while (true) {            
            if (1 == validFields[0]) {
                client[0] = getId();
            }
            if (1 == validFields[1]) {
                client[1] = getName();
            }             
            if (1 == validFields[2]) {
                client[2] = getContactNo();
            }
            if (1 == validFields[3]) {
                client[3] = getMail();
            }            
            validFields = clientController.findValidClientDetails(
                                                        client[0],
                                                        client[1],
                                                        client[2],
                                                        client[3]);
            for (validFieldsCount = 0 ; validFieldsCount < 4 ; validFieldsCount++){
                if (1 == validFields[validFieldsCount]) {
                    break;
                }
            }
            if (4 == validFieldsCount) {
                break;
            } else {
                System.out.println("Entered details have invalid entry\n"
                                    + "re-enter asked details");
            }            
        }
        return client;
    }
        
    /**
     * <p>
     * Gets the client id
     * </p>
     * @Return  client id, an unique entity for identification
     */
     private String getId() {
         System.out.println(ConstantString.CLIENT_ID_INPUT);      
         return input.next();
     }
     
     /**
     * <p>
     * Gets the client name
     * </p>
     * @Return  client name, not an unique entity
     */
     private String getName() {
         System.out.println(ConstantString.CLIENT_NAME_INPUT);      
         return input.next(); 
     }
     
     /**
     * <p>
     * Gets the client contact no
     * </p>
     * @Return  client contact no, an unique entity for identification
     */
     private String getContactNo() {
         System.out.println(ConstantString.CLIENT_CONTACT_NO_INPUT);
         return input.next();   
     }
     
     /**
     * <p>
     * Gets the client mail
     * </p>
     * @Return  client mail, an unique entity for identification
     */
     private String getMail() {
        System.out.println(ConstantString.CLIENT_MAIL_INPUT);
        return input.next();   
     }
     
    /**
     * <p>
     * Removes the client details by asking his id
     * Displays "invalid id" if the entered id not present
     * </p>
     */
    private void removeClientById() {
        System.out.println(ConstantString.CLIENT_ID_INPUT);
        String id = input.next();
        try {
            clientController.removeClientById(id);
            removeAddressById(id);
            System.out.println(ConstantString.CLIENT_DELETION); 
        } catch (MyException myException){
            System.out.println(myException.getMessage());
        }
    }
    
    /**
     * <p>
     * Displays the clientDetails by asking his id
     * Or displays "client not exixt" if entered id is invalid or not exist 
     * </p>
     */
    private void displayClientById() {     
        System.out.println(ConstantString.CLIENT_ID_INPUT);                                             
        System.out.println(clientController.getClientById(input.next()));
    } 
    
    /**
     * <p>
     * Displays all the client details 
     * </p>
     * @param   clients     List that holds the client class' objects
     */
    private void displayClients() {                                                  
        System.out.println(clientController.getAllClients());
    }
    
    /**
     * <p>
     * Displays options for updation of id, name, contactNo, projectId and mail
     * Or displays "client id not exist" if id not exist or invalid
     * </p>
     * @param    id    client id whose details are to be updated 
     */
    private void updateClientById() {
        boolean loopExitKey = true;        
        System.out.println(ConstantString.CLIENT_ID_INPUT);
        String id = input.next();
            while (loopExitKey) {
                System.out.println(id + " " + ConstantString.CLIENT_UPDATE_OPTIONS);
                try {
                    switch (input.next()) {
                        case "1" :
                            updateClientName(id);
                            break;
                        case "2" :
                            updateClientMail(id);
                            break;
                        case "3" :
                            updateClientContactNo(id);
                            break;
                        case "5" :
                            updateAddressById(id);
                            break;
                        case "6" :
                            System.out.println(ConstantString.CLIENT_ID_INPUT);
                            id = input.next();
                            break; 
                        case "7" :
                            loopExitKey = false;
                            break;
                        default :
                            break;
                    }
                } catch (MyException myException) {
                    System.out.println(myException.getMessage());
                }
            } 
    }                 
     
    /**
     * <p>
     * Updates the old client name with new one
     * </p>
     * @param     id     id of the client to be updated
     */
    private void updateClientName(String id) throws MyException {
        System.out.println(ConstantString.CLIENT_NAME_INPUT);
        clientController.updateClientName(id, input.next());
        System.out.println(ConstantString.CLIENT_NAME_UPDATION);
    }
    
 
    /**
     * <p>
     * Updates the old client contactNo with new one
     * Or displays "invalid contactNo" if entered contactNo is not valid
     * </p>
     * @param    id     id of the client to be updated
     */
    private void updateClientContactNo(String id) throws MyException {
        System.out.println(ConstantString.CLIENT_CONTACT_NO_INPUT);
        clientController.updateClientContactNo(id, input.next());
        System.out.println(ConstantString.CLIENT_CONTACT_NO_UPDATION); 
    }

    /**
     * <p>
     * Updates the old client mail with new one
     * Or display "invalid mail" if the entered mail is not valid
     * </p>
     * @param     id     id of the client to be updated
     */ 
     private void updateClientMail(String id) throws MyException {
         System.out.println(ConstantString.CLIENT_MAIL_INPUT);
         clientController.updateClientMail(id, input.next());
         System.out.println(ConstantString.CLIENT_MAIL_UPDATION);
    }
    
    /**
     * <p>
     * prints the string in argument
     * </p>
     */
    public void showErrorMessage(String messageToShow) {
        System.out.println(messageToShow);    
    }
    
    /**
     * <p> 
     * Gets the address details for the client and 
     * adds it to the client details
     * </p>
     * @param      id     id of the client, a unique entity for identification                   
     */
    public void addAddressToClient(String id) {
        try {
            System.out.println("Provide the permanent address details");
            String[] address = getAddress();
            clientController.addAddress(id, "P", address[0], address[1],
                                            address[2], address[3], address[4],     
                                            address[5], address[6], address[7]);
            System.out.println("Provide the current address for employee");
            address = getAddress();
            clientController.addAddress(id, "S", address[0], address[1],
                                            address[2], address[3], address[4],     
                                            address[5], address[6], address[7]);      
        } catch (MyException myException) {
            System.out.println(myException.getMessage());
        }
    }
    
    /**
     * <p> 
     * Removes the address details for the client by id
     * </p>
     * @param      id     id of the client, a unique entity for identification                   
     */
    public void removeAddressById(String id) {
        clientController.removeAddressById(id);
    }
    
    /**
     * <p> 
     * Gets the address details for the client and 
     * updates it with old details
     * </p>
     * @param      id     id of the client, a unique entity for identification                   
     */
    public void updateAddressById(String id) {
        String[] address = getAddress();
        clientController.updateAddressById(id, address[0], address[1], 
                                 address[2], address[3], address[4], address[5],       
                                            address[6], address[7], address[8]);
    }
    
    /**
     * <p>
     * Gets the client address
     * </p>
     * @return  returns the array that contains address details in order
     */
    private String[] getAddress() {
        String[] address = new String[9];
        Scanner reader = new Scanner(System.in);
        System.out.println("Door no?");
        address[1] = reader.nextLine();
        System.out.println("Landmark/ Apartement?");
        address[2] = reader.nextLine();
        System.out.println("Street Name?");
        address[3] = reader.nextLine();
        System.out.println("City?");
        address[4] = reader.nextLine();
        System.out.println("District?");
        address[5] = reader.nextLine();
        System.out.println("State?");
        address[6] = reader.nextLine();
        System.out.println("Country?");
        address[7] = reader.nextLine();
        System.out.println("Post code?");
        address[8] = reader.nextLine();
        return address;
    }
}
        
    
