package com.ideas2it.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.ideas2it.common.ConstantString;
import com.ideas2it.controller.EmployeeController;
import com.ideas2it.model.Address;
import com.ideas2it.exception.MyException;
import com.ideas2it.model.Employee;


/**
 * <p>
 * Makes following operations on employee detail    
 * Creates new, delete an employee, updates an employee detail, displays 
 * particular employee details, displays all the employee details   
 * </p>
 * author Jegatheesh Palanisamy
 * 26/07/2017
 */
public class EmployeeView {
    private EmployeeController employeeController = new EmployeeController();
    private Scanner input = new Scanner(System.in);
  
    /**
     * <p>
     * Displays options for add,remove,update,quit,display an employee to choose
     * Does the operation based on user input 
     * </p>
     */
    public void manageEmployee() {    
        EmployeeView employeeView = new EmployeeView();
        boolean loopExitKey = true;
        while (loopExitKey) {
            String id;
            System.out.println(ConstantString.EMPLOYEE_OPTIONS);        
            switch(input.next()) {
                case "1" :
                    employeeView.createEmployee();                    
                    break;
                case "2" : 
                    System.out.println(ConstantString.EMPLOYEE_ID_INPUT); 
                    id = input.next();
                    if (employeeView.removeEmployeeById(id)) {
                        employeeView.removeAddressById(id);
                    }
                    break;
                case "3" :  
                    employeeView.displayEmployeeById();
                    break;
                case "4" :
                    employeeView.displayEmployees();                    
                    break;
                case "5" :
                    employeeView.updateEmployeeById();
                    break;
                case "6" :
                    loopExitKey = false;
                    break;
                default:
                    break;
            }    
        }   
    }
    
    
    /** 
     * <p>
     * Gets name, id, designation, dob and mail of employee from user 
     * Creates the new employee  
     * </p>                                             
     */
    private void createEmployee() {
        String[] employee = getValidEmployeeDetails();        
        employeeController.createEmployee(employee[0],
                                          employee[1],
                                          employee[2],
                                          employee[3],
                                          employee[4]);
        System.out.println(ConstantString.EMPLOYEE_CREATION);       
        addAddressToEmployee(employee[0]);
    }
    
    /**
     * <p>
     * Gets the validated employee details 
     * </p>
     * @return array of employee details in order
     */
    private String[] getValidEmployeeDetails() {
        String[] employee = new String[5];
        int[] validFields = {1,1,1,1,1}; 
        while (true) {            
            if (validFields[0] == 1) {
                employee[0] = getId();
            }
            if (validFields[1] == 1) {
                employee[1] = getName();
            }             
            if (validFields[2] == 1) {
                employee[2] = getDesignation();
            }
            if (validFields[3] == 1) {
                employee[3] = getMail();
            }            
            if (validFields[4] == 1) {
                employee[4] = getDob();
            }
            validFields = employeeController.findValidEmployeeDetails(
                                                        employee[0],
                                                        employee[1],
                                                        employee[2],
                                                        employee[3],
                                                        employee[4]);
            int validFieldsCount;
            for (validFieldsCount = 0 ; validFieldsCount < 5 ; validFieldsCount++){
                if (validFields[validFieldsCount] == 1) {
                    break;
                }
            }
            if (5 == validFieldsCount) {
                break;
            } else {
                System.out.println("Entered details have invalid entry\n"
                                    + "re-enter asked details");
            }            
        }
        return employee;
    } 
    
    /**
     * <p>
     * Gets the employee id
     * </p>
     * @return employee id, an unique entity for identification
     */
    private String getId() {
        System.out.println(ConstantString.EMPLOYEE_ID_INPUT);
        return input.next();
    }
    
    /**
     * <p>
     * Gets the employee name
     * </p>
     * @return employee name, not an unique entity 
     */
    private String getName() {
        System.out.println(ConstantString.EMPLOYEE_NAME_INPUT);      
        return input.next();
    }
    
    /**
     * <p>
     * Gets the employee designation
     * </p>
     * @return employee designation,not an unique entity
     */
    private String getDesignation() {
        System.out.println(ConstantString.EMPLOYEE_DESIGNATION_INPUT);
        return input.next();
    }
    
    /**
     * <p>
     * Gets the employee mail
     * </p>
     * @return employee mail, an unique entity for identification
     */
    private String getMail() {
        System.out.println(ConstantString.EMPLOYEE_MAIL_INPUT);
        return input.next(); 
    }
    
    /**
     * <p>
     * Gets the employee id
     * </p>
     * @return employee dob,not an unique entity 
     */
    private String getDob() {
        System.out.println(ConstantString.EMPLOYEE_DOB_INPUT);
        return input.next();
    }
    
    /**
     * <p>
     * Removes the employee details by asking his id
     * Displays "invalid id" if the entered id not present
     * </p>
     */
    private boolean removeEmployeeById(String id) {
        if (employeeController.removeEmployeeById(id)) {
           System.out.println(ConstantString.EMPLOYEE_DELETION);
           return true; 
        } else {
            System.out.println(ConstantString.EMPLOYEE_NOT_EXIST);
            return true;
        }
    }
    
    /**
     * <p>
     * Displays the employeeDetails by asking his id
     * Or displays "employee not exixt" if entered id is invalid or not exist 
     * </p>
     */
    private void displayEmployeeById() {     
        System.out.println(ConstantString.EMPLOYEE_ID_INPUT);     
        String id = input.next();                                        
        System.out.println(employeeController.getEmployeeById(id));
        List<Address> addresses = employeeController.getAddressById(id);
        for (Address address : addresses) {
            System.out.println(address);
        }
    } 
    
    /**
     * <p>
     * Displays all the employee details 
     * </p>
     * @param   employees     List that holds the employee class' objects
     */
    private void displayEmployees() {                                                  
        List<Employee> employees = employeeController.getAllEmployees();
        List<Address> addresses = employeeController.getAllAddresses();
        if (null != employees) {
            for (Employee employee : employees) {
                System.out.println(employee);
                for (Address address : addresses) {
                    if (employee.getId().equals(address.getId())) {
                        System.out.println(address);
                    }
                }
            }
        }
    }
    
    /**
     * <p>
     * Displays options for updation of id, name, designation, dob and mail
     * Or displays "employee id not exist" if id not exist or invalid
     * </p>
     * @param    id    employee id whose details are to be updated 
     */
    private void updateEmployeeById() {
        boolean loopExitKey = true;        
        System.out.println(ConstantString.EMPLOYEE_ID_INPUT);
        String id = input.next();
            while (loopExitKey) {
                System.out.println(ConstantString.UPDATE_OPTIONS + id);
                switch (input.next()) {
                    case "1" :
                        updateEmployeeName(id);
                        break;
                    case "2" :
                        updateEmployeeMail(id);
                        break;
                    case "3" :
                        updateEmployeeDesignation(id);
                        break;
                    case "4" :
                        updateEmployeeDob(id);
                        break;
                    case "5" :
                        updateAddressById(id);
                        break;
                    case "6" :
                        System.out.println(ConstantString.EMPLOYEE_ID_INPUT);
                        id = input.next();
                        break; 
                    case "7" :
                        loopExitKey = false;
                        break;
                }
            } 
    }                 
     
    /**
     * <p>
     * Updates the old employee name with new one
     * </p>
     * @param     id     id of the employee to be updated
     */
    private void updateEmployeeName(String id) {
        System.out.println(ConstantString.EMPLOYEE_NAME_INPUT);
        if (employeeController.updateEmployeeName(id, input.next())) {
            System.out.println(ConstantString.NAME_UPDATION);
        } else {
            System.out.println(ConstantString.INVALID_NAME);
        }
    }
    
    /**
     * <p>
     * Updates old employee dob with new one 
     * Or displays "invalid dob" if entered dob is not valid
     * </p>
     * @param    id      id of the employee to be updated
     */
    private void updateEmployeeDob(String id) {
        System.out.println(ConstantString.EMPLOYEE_DOB_INPUT);
        if (employeeController.updateEmployeeDob(id, input.next())) {
            System.out.println(ConstantString.DOB_UPDATION);
        } else {
            System.out.println(ConstantString.INVALID_DOB);
        }
    }
 
    /**
     * <p>
     * Updates the old employee designation with new one
     * Or displays "invalid designation" if entered designation is not valid
     * </p>
     * @param    id     id of the employee to be updated
     */
    private void updateEmployeeDesignation(String id) {
        System.out.println(ConstantString.EMPLOYEE_DESIGNATION_INPUT);
        if (employeeController.updateEmployeeDesignation(id, input.next())) {
            System.out.println(ConstantString.DESIGNATION_UPDATION); 
        } else {
            System.out.println(ConstantString.INVALID_DESIGNATION);
        }
    }

    /**
     * <p>
     * Updates the old employee mail with new one
     * Or display "invalid mail" if the entered mail is not valid
     * </p>
     * @param     id     id of the employee to be updated
     */ 
     private void updateEmployeeMail(String id) {
         System.out.println(ConstantString.EMPLOYEE_MAIL_INPUT);
         if (employeeController.updateEmployeeMail(id, input.next())) {
             System.out.println(ConstantString.MAIL_UPDATION);
         } else {
             System.out.println(ConstantString.INVALID_MAIL);
         }
    }
    
    /**
     * <p>
     * prints the string in argument
     * </p>
     */
    public void showErrorMessage(String messageToShow) {
        System.out.println(messageToShow);    
    }
    
    /**
     * <p> 
     * Gets the address details for the employee and 
     * adds it to the client details
     * </p>
     * @param      id     id of the employee, a unique entity for identification                   
     */
    public void addAddressToEmployee(String id) {
        try {
            System.out.println("Provide the permanent address details");
            String[] address = getAddress();
            employeeController.addAddress(id, "P", address[0], address[1],
                                            address[2], address[3], address[4],     
                                            address[5], address[6], address[7]);
            System.out.println("Provide the current address for employee");
            address = getAddress();
            employeeController.addAddress(id, "S", address[0], address[1],
                                            address[2], address[3], address[4],     
                                            address[5], address[6], address[7]);     
        } catch (MyException myException) {
            System.out.println(myException.getMessage());
        }
    }
    
    
    /**
     * <p> 
     * Removes the address details for the employee by id
     * </p>
     * @param      id     id of the employee, a unique entity for identification                   
     */
    public void removeAddressById(String id) {
        try {
            employeeController.removeAddressById(id);
        } catch (MyException myException) {
            System.out.println(myException.getMessage());
        }
    }
    
    /**
     * <p> 
     * Gets the address details for the employee and 
     * updates it with old details
     * </p>
     * @param      id     id of the client, a unique entity for identification                   
     */
    public void updateAddressById(String id) {
        String addressType;
        while(true) {
            System.out.println("permanent/current address(P/S)?");
            addressType = input.next();
            if ((addressType.equals("P")) || (addressType.equals("S"))) {
                break;
             }
            System.out.println("Please provide a valid detail");
        }
        String[] address = getAddress();
        employeeController.updateAddressById(id, addressType, address[0], 
                                         address[1], address[2], address[3],
                              address[4], address[5], address[6], address[7]);
    } 
    
    /**
     * <p>
     * Gets the employee address
     * </p>
     * @return  returns the array that contains address details in order
     */
    private String[] getAddress() {
        String[] address = new String[9];
        Scanner reader = new Scanner(System.in);
        System.out.println("Door no?");
        address[0] = reader.nextLine();
        System.out.println("Landmark/ Apartement?");
        address[1] = reader.nextLine();
        System.out.println("Street Name?");
        address[2] = reader.nextLine();
        System.out.println("City?");
        address[3] = reader.nextLine();
        System.out.println("District?");
        address[4] = reader.nextLine();
        System.out.println("State?");
        address[5] = reader.nextLine();
        System.out.println("Country?");
        address[6] = reader.nextLine();
        System.out.println("Post code?");
        address[7] = reader.nextLine();
        return address;
    }       
}
        
    
