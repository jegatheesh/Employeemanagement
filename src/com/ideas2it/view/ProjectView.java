package com.ideas2it.view;

import java.util.Scanner;

import com.ideas2it.common.ConstantString;
import com.ideas2it.controller.ProjectController;
import com.ideas2it.exception.MyException;

/**
 * <p>
 * Makes following operations on project detail    
 * Creates new, delete an project, updates an project detail, displays 
 * particular project details, displays all the project id   
 * </p>
 * author Jegatheesh Palanisamy
 * 20/07/2017
 */
public class ProjectView {
    private static ProjectController projectController = new ProjectController();    
    private static Scanner input = new Scanner(System.in);
   
    /**
     * <p>
     * Displays options for add,remove,update,quit,display an project to choose
     * Does the operation based on user input 
     * </p>
     */
    public void manageProject() {        
        boolean loopExitKey = true; 
        ProjectView projectView = new ProjectView();                     
        while (loopExitKey) {
            System.out.println(ConstantString.PROJECT_OPTIONS);     
            switch(input.next()) {
                case "1" :
                    projectView.createProject ();                                                               
                    break;
                case "2" : 
                    projectView.removeProjectById();
                    break;
                case "3" :  
                    projectView.displayProjectById();
                    break;
                case "4" :
                    projectView.displayProjects();                    
                    break;
                case "5" :
                    projectView.updateProjectById();
                    break;
                case "6" :
                    projectView.assignEmployee();
                    break;
                case "7" :
                    projectView.removeEmployeeFromProject();
                    break;
                case "8" :
                    projectView.displayEmployeesByProjectId();
                    break;
                case "9" :
                    loopExitKey = false;
                default:
                    break;
            } 
        }
    }

    /** 
     * <p>
     * Gets the name, id, designation, dob and mail of project from user as string
     * Displays the invalid details if details do not match the pattern   
     * </p>                                              
     */
    private void createProject() {
        System.out.println(ConstantString.PROJECT_ID_INPUT); 
        String id = input.next();            
        System.out.println(ConstantString.PROJECT_NAME_INPUT);      
        String name = input.next();        
        System.out.println(ConstantString.PROJECT_CLIENT_INPUT);
        String clientId = input.next();
        System.out.println(ConstantString.PROJECT_DOMAIN_INPUT);
        String projectDomain = input.next();
        try {
            projectController.createProject(id, name, clientId, projectDomain);
            System.out.println(ConstantString.PROJECT_CREATION);
            System.out.println("Want to create new employee to this project"
                                                  + "yes = press 'y'");
            while (true) {
            if (!input.next().equals("y")) {
                break;    
            } else {
                createEmployee(id);
                System.out.println("Want to create another employee");
            }
            }
        } catch (MyException myException) {
            System.out.println(myException.getMessage());
        }  
    }
    
    /** 
     * <p>
     * Gets name, id, designation, dob and mail of employee from user 
     * Creates the new employee  
     * </p>                                             
     */
    private void createEmployee(String projectId) {
        String[] employee = getValidEmployeeDetails();        
        projectController.createEmployee(employee[0],
                                          employee[1],
                                          employee[2],
                                          employee[3],
                                          employee[4]);
        projectController.assignEmployee(employee[0], projectId);
        System.out.println(ConstantString.EMPLOYEE_CREATION);       
        addAddressToEmployee(employee[0]);
    }
    
    /**
     * <p>
     * Gets the validated employee details 
     * </p>
     * @return array of employee details in order
     */
    private String[] getValidEmployeeDetails() {
        String[] employee = new String[5];
        int[] validFields = new int[5]; 
        while (true) {            
            if (validFields[0] == 1) {
                employee[0] = getEmployeeId();
            }
            if (validFields[1] == 1) {
                employee[1] = getEmployeeName();
            }             
            if (validFields[2] == 1) {
                employee[2] = getEmployeeDesignation();
            }
            if (validFields[3] == 1) {
                employee[3] = getEmployeeMail();
            }            
            if (validFields[4] == 1) {
                employee[4] = getEmployeeDob();
            }
            validFields = projectController.findValidEmployeeDetails(
                                                        employee[0],
                                                        employee[1],
                                                        employee[2],
                                                        employee[3],
                                                        employee[4]);
            int validFieldsCount;
            for (validFieldsCount = 0 ; validFieldsCount < 5 ; validFieldsCount++){
                if (validFields[validFieldsCount] == 1) {
                    break;
                }
            }
            if (5 == validFieldsCount) {
                break;
            } else {
                System.out.println("Entered details have invalid entry\n"
                                    + "re-enter asked details");
            }            
        }
        return employee;
    }
    
    /**
     * <p>
     * Gets the employee id
     * </p>
     * @return employee id, an unique entity for identification
     */
    private String getEmployeeId() {
        System.out.println(ConstantString.EMPLOYEE_ID_INPUT);
        return input.next();
    }
    
    /**
     * <p>
     * Gets the employee name
     * </p>
     * @return employee name, not an unique entity 
     */
    private String getEmployeeName() {
        System.out.println(ConstantString.EMPLOYEE_NAME_INPUT);      
        return input.next();
    }
    
    /**
     * <p>
     * Gets the employee designation
     * </p>
     * @return employee designation,not an unique entity
     */
    private String getEmployeeDesignation() {
        System.out.println(ConstantString.EMPLOYEE_DESIGNATION_INPUT);
        return input.next();
    }
    
    /**
     * <p>
     * Gets the employee mail
     * </p>
     * @return employee mail, an unique entity for identification
     */
    private String getEmployeeMail() {
        System.out.println(ConstantString.EMPLOYEE_MAIL_INPUT);
        return input.next(); 
    }
    
    /**
     * <p>
     * Gets the employee id
     * </p>
     * @return employee dob,not an unique entity 
     */
    private String getEmployeeDob() {
        System.out.println(ConstantString.EMPLOYEE_DOB_INPUT);
        return input.next();
    }
    
     /**
     * <p> 
     * Gets the address details for the employee and 
     * adds it to the client details
     * </p>
     * @param      id     id of the employee, a unique entity for identification                   
     */
    public void addAddressToEmployee(String id) {
        try {
            System.out.println("Provide the permanent address details");
            String[] address = getAddress();
            projectController.addAddress(id, "P", address[0], address[1],
                                            address[2], address[3], address[4],     
                                            address[5], address[6], address[7]);
            System.out.println("Provide the current address for employee");
            address = getAddress();
            projectController.addAddress(id, "S", address[0], address[1],
                                            address[2], address[3], address[4],     
                                            address[5], address[6], address[7]);     
       } catch (MyException myException) {
           System.out.println(myException.getMessage());
       }
    }
    
    /**
     * <p>
     * Gets the employee address
     * </p>
     * @return  returns the array that contains address details in order
     */
    private String[] getAddress() {
        String[] address = new String[9];
        Scanner reader = new Scanner(System.in);
        System.out.println("Door no?");
        address[0] = reader.nextLine();
        System.out.println("Landmark/ Apartement?");
        address[1] = reader.nextLine();
        System.out.println("Street Name?");
        address[2] = reader.nextLine();
        System.out.println("City?");
        address[3] = reader.nextLine();
        System.out.println("District?");
        address[4] = reader.nextLine();
        System.out.println("State?");
        address[5] = reader.nextLine();
        System.out.println("Country?");
        address[6] = reader.nextLine();
        System.out.println("Post code?");
        address[7] = reader.nextLine();
        return address;
    }
    
    /**
     * <p>
     * Removes the project details by asking his id
     * Display "invalid id" if the entered id not present
     * </p>
     */
    private void removeProjectById() { 
        System.out.println(ConstantString.PROJECT_ID_INPUT);              
        try {
            projectController.removeProjectById(input.next());
            System.out.println(ConstantString.PROJECT_DELETION);
        } catch (MyException myException) { 
            System.out.println(myException.getMessage());
        }
    }
    
    /**
     * <p>
     * Displays the projectDetails by asking his id
     * Or displays "project not exixt" if entered id is invalid or not exist 
     * </p>
     */
    private void displayProjectById() {
        System.out.println(ConstantString.PROJECT_ID_INPUT);
        System.out.println(projectController.getProjectById(input.next()));
    } 
    
    /**
     * <p>
     * Displays all the project details in list
     * </p>
     * @param   projects     List that holds the project details objects
     */
    private void displayProjects() {
        System.out.println(projectController.getAllProjects());
    }
    
    /**
     * <p>
     * Displays options for updation of id, name, designation, dob and mail
     * Or displays "project id not exist" if id not exist or invalid
     * </p>
     * @param    id    project id whose details are to be updated 
     */
     private void updateProjectById() {
        ProjectView projectView = new ProjectView();
        boolean loopExitKey = true;
        System.out.println(ConstantString.PROJECT_ID_INPUT);
        String id = input.next();
        while (loopExitKey) { 
            System.out.println(ConstantString.PROJECT_UPDATE_PAGE 
                               + id + ConstantString.PROJECT_UPDATE_OPTIONS);               
            switch (input.next()) {
                case "1" :
                    updateProjectName(id);
                    break;
                case "2" :
                    updateProjectDomain(id);
                    break;
                case "3" :
                    System.out.println(ConstantString.PROJECT_ID_INPUT);
                    id = input.next();
                    break;
                case "4" :
                    loopExitKey = false;
                    break;
            }
        }             
    } 
    
     
    /**
     * Updates the old project name with new one
     * </p>
     * @param id     id of the project to be updated
     */
    private void updateProjectName(String id) throws MyException {
        System.out.println(ConstantString.PROJECT_NAME_INPUT);
        projectController.updateProjectName(id,input.next());
        System.out.println(ConstantString.PROJECT_NAME_UPDATION);
    }
    
    /**
     * <p>
     * Updates old project client with new one 
     * Or displays "invalid client" if entered client is not valid
     * </p>
     * @param  id      id of the project to be updated
     */
    private void updateProjectDomain(String id) throws MyException {
        System.out.println(ConstantString.PROJECT_CLIENT_INPUT);
        projectController.updateProjectDomain(id,input.next());
        System.out.println(ConstantString.PROJECT_CLIENT_UPDATION);
    } 
        
    /**
     * <p>
     * Assigns employee to the project id
     * Asks for another assignement after the first
     * </p>
     */
    private void assignEmployee() {
        System.out.println(ConstantString.PROJECT_ID_INPUT);
        String projectId = input.next();
        System.out.println(ConstantString.SHOW_ALL_EMPLOYEES);
        if (input.next().equals("y")) {
            showAllEmployees();
        }
        while (true) {            
            System.out.println(ConstantString.EMPLOYEE_ID_INPUT);
            try {
                projectController.assignEmployee(input.next(), projectId);
                System.out.println(ConstantString.EMPLOYEE_ASSIGNMENT);                
            } catch (MyException myException) {
                System.out.println(myException.getMessage());
            }
            System.out.println(ConstantString.ANOTHER_EMPLOYEE_ASSIGNMENT);
            if (!input.next().equals("y")) {
                break;
            }
        }
    }
    
    /**
     * <p>
     * Displays all employees and their details
     * </p>
     */
    private void showAllEmployees() {
        System.out.println(projectController.getAllEmployees());
    }    
    
    /**
     * <p>
     * Removes an employee from project taggint th employees project id to null
     * </p>
     */
    private void removeEmployeeFromProject () {
        System.out.println(ConstantString.EMPLOYEE_DELETION);
        String employeeId = input.next(); 
        System.out.println(ConstantString.EMPLOYEE_ID_INPUT);
        String projectId = input.next();
        try {
            projectController.removeEmployeeFromProject(projectId, employeeId);
            System.out.println(ConstantString.EMPLOYEE_DELETION);
        } catch (MyException myException) {
            System.out.println(myException.getMessage());
        }
    }   
    
    /**
     * <p>
     * Displays all the employees for particulat project id
     * </p>
     */
    private void displayEmployeesByProjectId() {
        System.out.println(ConstantString.PROJECT_ID_INPUT);
        System.out.println(projectController.
                                        getAllEmployeesByProjectId(input.next()));  
    }
    
    /**
     * <p>
     * prints the string in argument
     * </p>
     * @param messageTOShow message to display
     */
    public void showErrorMessage(String messageToShow) {
        System.out.println(messageToShow);    
    }
}
        
               
        
        
    
