package com.ideas2it.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.ideas2it.exception.MyException;

/**
 * <p>
 * Provides database connection to various databases
 * </p>
 */
public class DatabaseConnectivity {
    private static DatabaseConnectivity databaseConnectivity;
    public static Connection connection ;
    private static final String url = "jdbc:mysql://localhost/"
                       + "EMPLOYEE_MANAGEMENT?autoReconnect=true&useSSL=false";
    private static final String user = "root";
    private final String password = "root";
    
    private DatabaseConnectivity() {}
   
    /**
     * <p>
     * Establishes the connection to the database using url
     * <p/>
     * @return      a connection object that holds connection to the database
     */
    public Connection connectToDataBase() throws SQLException{        
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException sqlException) {
            throw sqlException;
        }
        return connection;
    }
    
    /**
     * <p>
     * returns the object instance of this connectivity
     * <p/>
     * @return     object for the databaseConnectivity 
     */
    public static DatabaseConnectivity getInstance() {
        if (null == databaseConnectivity) {
            synchronized(DatabaseConnectivity.class) {
                if (null == databaseConnectivity) {
                    databaseConnectivity = new DatabaseConnectivity();
                }
            }
        }
        return databaseConnectivity;   
    } 
    
    /**
     * <p>
     * Closes the connection after ckecking if it is not null
     * <p/>
     */
    public void closeConnection() throws SQLException {
        try {
            if (null != connection) {
                connection.close();
            }
        } catch (SQLException sqlException) {
            throw sqlException;
        }
    }
}
