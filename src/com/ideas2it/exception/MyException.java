package com.ideas2it.exception;

/**
 * <p>
 * Provides custom exception handling options
 * </p>
 */
public class MyException extends RuntimeException {
   
    /**
     * <p>
     * Constructor that stores the given message
     * </p>
     * @param  message   exception message to be displayed
     */
    public MyException (String message) {
        super(message);
    } 
    
    /**
     * <p>
     * A constructor with no arguments
     * </p>
     */       
    public MyException () {}
    
    /**
     * <p>
     * Constructor that passes the message and throwable 
     * </p>
     * @param   message   exception message to be displayed
     * @param   cause     exception object that throws exception
     */
    public MyException (String message, Throwable cause) {
        super(message, cause);
    }   
    
    /**
     * <p>
     * Constructor that passes nly throwable
     * </p>
     * @param    cause  exception object that throws exception
     */ 
    public MyException (Throwable cause) {
        super(cause);
    }  
}
