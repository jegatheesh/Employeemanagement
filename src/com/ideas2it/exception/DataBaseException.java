package com.ideas2it.exception;

import com.ideas2it.exception.MyException;

public class DataBaseException extends MyException{

    public DataBaseException() {}
    
    public MyException throwDbConnectionFailure() {
        return new MyException("Database connection failure : Can't establish connection!"
            + " Recheck the details");
    }
    
    public MyException throwDbDisconnected() {
        return new MyException("Database connection lost : Try reconnecting ");
    }
}
