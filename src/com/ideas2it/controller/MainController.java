package com.ideas2it.controller;

import java.util.Scanner;
import com.ideas2it.view.EmployeeView;
import com.ideas2it.view.ProjectView;
import com.ideas2it.view.ClientView;

/**
 <p>
 * Directs the user to project or employee manage page
 * </p>
 * author Jegatheesh palanisamy
 * 20/07/2017
 */
public class MainController {
    
    /**
     * <p>
     * Displays options to select between project and employee management
     * </p>
     */
    public static void main(String[] args) {
        while (true) {
            Scanner input = new Scanner(System.in);
            System.out.println("1-go to employee management\n"
                             + "2-go to project management\n"
                             + "3-go to client management\n"
                             + "4-quit");          
            switch (input.next()) {
                case "1" : 
                    EmployeeView employeeView = new EmployeeView();
                    employeeView.manageEmployee();
                    break;
                case "2" :                  
                    ProjectView projectView = new ProjectView();
                    projectView.manageProject();
                    break;
                case "3" :
                    ClientView clientView = new ClientView();
                    clientView.manageClient();
                    break;
                case "4" :
                    System.exit(0);
                    break;  
                default :
                    break; 
            }
        }
    }   
}
