package com.ideas2it.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.ideas2it.common.ConstantString;
import com.ideas2it.exception.MyException;
import com.ideas2it.model.Client;
import com.ideas2it.service.AddressService;
import com.ideas2it.service.ClientService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.service.impl.AddressServiceImpl;
import com.ideas2it.service.impl.ClientServiceImpl;
import com.ideas2it.service.impl.ProjectServiceImpl;
import com.ideas2it.view.ClientView;

/**
 * <p>
 * Controls the flow between the view and the service class
 * Provides the methods create, delete, update, fetching the client details
 * </p> 
 * author Jegatheesh palanisamy
 * 27/07/2017
 */
public class ClientController {
    private ClientService clientService = new ClientServiceImpl();
    
    /**
     * <p>
     * Adds new client detail to client table
     * </p>
     * @param    id          id of client, an unique entity for identification 
     * @param    name        client name, not necessarily unique
     * @param    contactNo client contactNo, not necessarily unique
     * @param    mail        client mail an unique entity for each client
     * @param    projectId         client projectId to be added
     */
    public void createClient(String id, String name, String contactNo, 
                                             String mail) throws MyException {
             clientService.insertClient(id, name, contactNo, mail);
                                                                       
    }
    
    /** 
     * <P>
     * Creates new project that holds the details name, id, and client
     * Displays the invalid details if details do not match the necessary format
     * </p> 
     * @param    id          id of project, an unique entity for identification
     * @param    name        project name not necessarily unique
     * @param    client      project client not necessarily unique                                             
     */
    public void createProject(String id, String name, String clientId,
                                      String projectDomain) throws MyException { 
        ProjectService projectService = new ProjectServiceImpl(); 
        projectService.createProject(id, name, clientId, projectDomain);  
    }
    
    /**
     * <p>
     * Finds the client details which are valid
     * </p>
     * @Return   returns an int that denotes tha valid and invalid details
     */
    public int[] findValidClientDetails(String id, String name, 
                                              String contactNo, String mail) {
        return clientService.findValidClientDetails(id, name, contactNo, mail);
                                                            
    } 
    
    /**
     * <p>
     * adds the address details of the client
     * </p>
     * @param  id  id of client, an unique entity for identification 
     * @param  addressType   unique for each client, defines the address' type
     * @param  doorNo    an address detail, does not need to be unique
     * @param  landmark  an address detail, does not need to be unique
     * @param  street    an address detail, does not need to be unique
     * @param  city      an address detail, does not need to be unique
     * @param  district  an address detail, does not need to be unique
     * @param  state     an address detail, does not need to be unique
     * @param  country   an address detail, does not need to be unique
     * @param  postcode  an address detail, does not need to be unique
     * @return  true if address is added else false
     */
    public void addAddress(String id, String addressType, String doorNo,
                                        String landMark, String street,
                                        String city, String district,
                                        String state, String country, 
                                        String postCode) throws MyException {
       AddressService addressService = new AddressServiceImpl();
       addressService.addAddress(id, addressType, doorNo, landMark,
                              street, city, district, state, country, postCode);
       
    }
    
    /**
     * <p>
     * Removes address of client by id
     * </p>
     * @param id  id of client, an unique entity for identification 
     * @return  true if address is removed else false
     */
    public void removeAddressById(String id) throws  MyException {
        AddressService addressService = new AddressServiceImpl();
            addressService.removeAddressById(id);
    }
    
    /**
     * <p>
     * adds the address details of the client
     * </p>
     * @param  id  id of client, an unique entity for identification 
     * @param  addressType   unique for each client, defines the address' type
     * @param  doorNo    an address detail, does not need to be unique
     * @param  landmark  an address detail, does not need to be unique
     * @param  street    an address detail, does not need to be unique
     * @param  city      an address detail, does not need to be unique
     * @param  district  an address detail, does not need to be unique
     * @param  state     an address detail, does not need to be unique
     * @param  country   an address detail, does not need to be unique
     * @param  postcode  an address detail, does not need to be unique
     * @return  true if address is updated else false
     */
    public boolean updateAddressById(String id, String addressType, 
                                    String doorNo, String landMark,
                                    String street, String city, String district,
                                    String state, String country, 
                                                             String postCode) {
        AddressService addressService = new AddressServiceImpl();
        try {
            return addressService.updateAddressById(id, addressType, doorNo, 
                                                     landMark, street, city,  
                                            district, state, country, postCode);
        } catch (MyException myException) {
            ClientView clientView = new ClientView();
            clientView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        }
        return true;  
    }
    
    /**
     * <p>
     * Deletes particular client detail by client id 
     * </p>
     * @param   id   id of client, an unique entity for identification 
     * @return  true if client is removed else false
     */ 
    public void removeClientById(String id) throws MyException {
        clientService.removeClientById(id);
    }
    
    /**
     * <p>
     * Gets the details of particular client detail by client id 
     * </p>
     * @param     id     id of client, an unique entity for identification 
     * @return    client details of the client id or error message 
     *            if detail is null
     */
    public String getClientById(String id) {
        Client client = null;
        try {                                                  
            client = clientService.getClientById(id);
        } catch (MyException myException) {
            ClientView clientView = new ClientView();
            clientView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        } 
        if (null != client) {                 
            return client.toString();  
        }
        else {
            return ConstantString.EMPLOYEE_NOT_EXIST;       
        }
    } 
    
    /**
     * <p>
     * Gets all clientdetail and returns 
     * </p>
     * @return   returns all the clients details or error message
     *           if details are null 
     */
    public String getAllClients() {                                                  
        String clientsDetails = "\n";
        try {
            List<Client> clients = clientService.getAllClients();
            if (null != clients) {
                for (Client client : clients) {
                    clientsDetails = clientsDetails + client.toString();
                }
                return clientsDetails;
            } else {
                return ConstantString.NO_EMPLOYEES;
            }
        } catch (MyException myException) {
            ClientView clientView = new ClientView();
            clientView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);         
        }
        return ""; 
    }
    
    /**
     * <p>
     * Replaces an clients old name by new one
     * </p>
     * @param    id        id of client, an unique entity for identification 
     * @param    newName     holds the client's new name
     */
    public void updateClientName(String id , String name) throws MyException {
        clientService.updateClientName(id, name);
    }
    
    /**
     * <p>
     * Replaces an clients old contactNo by new one
     * </p>
     * @param    id                 id of client, an unique entity for 
     *                              identification 
     * @param    newContactNo     holds the client's new contactNo
     */
    public void updateClientContactNo(String id, String contactNo) throws 
                                                                 MyException {
        clientService.updateClientContactNo(id, contactNo);
    }
    
    /**
     * <p>
     * Replaces an clients old mail by new one
     * </p>
     * @param    id        id of client, an unique entity for identification 
     * @param    newMail   holds the client's new mail
     */ 
    public void updateClientMail(String id, String mail) throws MyException {
        clientService.updateClientMail(id, mail);
    }
}
