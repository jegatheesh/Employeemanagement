package com.ideas2it.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.ideas2it.common.ConstantString;
import com.ideas2it.exception.MyException;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.service.AddressService;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.service.impl.AddressServiceImpl;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.service.impl.ProjectServiceImpl;
import com.ideas2it.view.ProjectView;

/**
 * Makes following operations on project detail    
 * Creates new, delete an project, updates an project detail, displays 
 * particular project details, displays all the project id   
 * author Jegatheesh Palanisamy
 * 20/07/2017
 */
public class ProjectController {
    private Scanner input = new Scanner(System.in);
    private ProjectService projectService = new ProjectServiceImpl();    
 
    /** 
     * <P>
     * Creates new project that holds the details name, id, and client
     * Displays the invalid details if details do not match the necessary format
     * </p> 
     * @param    id          id of project, an unique entity for identification
     * @param    name        project name not necessarily unique
     * @param    client      project client not necessarily unique                                             
     */
    public void createProject(String id, String name, String clientId,
                                      String projectDomain) throws MyException {  
        projectService.createProject(id, name, clientId, projectDomain);  
    }
    
    /**
     * <p>
     * Adds new employee detail to employee table
     * </p>
     * @param    id          id of employee, an unique entity for identification 
     * @param    name        employee name, not necessarily unique
     * @param    designation employee designation, not necessarily unique
     * @param    mail        employee mail an unique entity for each employee
     * @param    dob         employee dob to be added
     */
    public boolean createEmployee(String id, String name, 
                                  String designation, String mail, String dob) {
        EmployeeService employeeService = new EmployeeServiceImpl();
        return employeeService.insertEmployee(id, name, designation, mail,
                                                                       dob);
    }
    
    /**
     * <p>
     * Finds the employee details which are valid
     * </p>
     * @Return   returns an int that denotes tha valid and invalid details
     */
    public int[] findValidEmployeeDetails(String id, String name, 
                                  String designation, String mail, String dob) {
        EmployeeService employeeService = new EmployeeServiceImpl();
        return employeeService.findValidEmployeeDetails(id, name, designation, 
                                                                     mail, dob);
    } 
    
    /**
     * <p>
     * adds the address details of the employee
     * </p>
     * @param  id  id of employee, an unique entity for identification 
     * @param  addressType   unique for each client, defines the address' type
     * @param  doorNo    an address detail, does not need to be unique
     * @param  landmark  an address detail, does not need to be unique
     * @param  street    an address detail, does not need to be unique
     * @param  city      an address detail, does not need to be unique
     * @param  district  an address detail, does not need to be unique
     * @param  state     an address detail, does not need to be unique
     * @param  country   an address detail, does not need to be unique
     * @param  postcode  an address detail, does not need to be unique
     * @return  true if address is updated else false
     */
    public void addAddress(String id, String addressType, String doorNo,
                                        String landMark, String street,
                                        String city, String district,
                                        String state, String country, 
                                        String postCode) {
        AddressService addressService = new AddressServiceImpl();
        addressService.addAddress(id, addressType, doorNo, landMark,   
                              street, city, district, state, country, postCode);
    }
    
    /**
     * <p>
     * Removes the project details by provided the project id
     * Display "invalid id" if the entered id not present
     * </p>
     * @param   id   id of project, an unique entity for identification
     */
    public void removeProjectById(String id) throws MyException {              
        projectService.removeProjectById(id);
    }
    
    /**
     * <p>
     * Gets the projectDetails by asking his id
     * Or displays "project not exixt" if entered id is invalid or not exist
     * </p> 
     * @param     id      id of project, an unique entity for identification
     * @return    returns error string if no project present by this id else
     *            returns employee detail as string
     */
    public String getProjectById(String id) {
        try {
            Project project = projectService.getProjectById(id);
         
            if (null != project) {                 
                return project.toString();
            }
            else {
                return ConstantString.INVALID_PROJECT_ID;
            }
        } catch (MyException myException) {
            ProjectView projectView = new ProjectView();
            projectView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        }  
        return null;
    } 
    
    /**
     * <p>
     * Gets all the projects details
     * </p>
     * @return   all the project details as string 
     *           or error message if the employee detail is null
     */
    public String getAllProjects() {
        List<Project> projects = null;
        try {
            projects = projectService.getAllProjects();
        } catch (MyException myException) {
            ProjectView projectView = new ProjectView();
            projectView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        }
        if (null != projects) { 
            String projectDetails = "\n";
            for (Project project : projects) {
                projectDetails = projectDetails + project.toString();
            }
            return projectDetails;
        }
        return ConstantString.NO_PROJECTS;
        
    }
     
    /**
     * <p>
     * Updates the old project name with new one
     * </p>
     * @param id     id of project, an unique entity for identification
     * @param name   new name of project
     */
    public void updateProjectName(String id , String name) 
                                                            throws MyException {
        projectService.updateProjectName(id, name);
    }
    
    /**
     * <p>
     * Updates old project client with new one 
     * Or displays "invalid client" if entered client is not valid
     * </p>
     * @param  id      id of project, an unique entity for identification
     * @param  client  new client of project
     */
    public void updateProjectDomain(String id, String projectDomain) 
                                                            throws MyException {
         projectService.updateProjectDomain( id, projectDomain);
    } 
       
    /**
     * <p>
     * Assigns employee to the project id
     * </p>
     * @param      projectId   project id for which employees are assigned
     * @param      employee to be assigned
     */
    public void assignEmployee(String employeeId,String projectId) 
                                                        throws MyException {
        projectService.assignEmployee(input.next(), projectId);
    }
    
    /**
     * <p>
     * Gets all employees' details 
     * </p>
     * @return    All employees details as string or null if there is no 
     *            employee detail stored
     */
    public String getAllEmployees() {
        EmployeeService employeeService = new EmployeeServiceImpl();
        try {
            List<Employee> employees = employeeService.getAllEmployees();
            if (null != employees) {
                String employeesDetails = "\n";
                for (Employee employee : employees) {
                    employeesDetails = employeesDetails 
                                                   + employee.toString() + "\n";
                }
                return employeesDetails;
            }
            return "No employees present";
        } catch (MyException myException) {
            ProjectView projectView = new ProjectView();
            projectView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        } 
        return ""; 
    }
      
    /**
     * <p>
     * Removes an employee from project provides the employee and project ids.
     * </p>
     * @param    projectId   id of project, an unique entity for identification
     * @param    employeeId  id of employee, an unique entity for identification
     */
    public void removeEmployeeFromProject (String projectId, String employeeId)
                                                           throws MyException {
        projectService.removeEmployeeFromProject(employeeId, projectId);
    }   
    
    /**
     * <p>
     * Gets  all the employees for particular project id
     * </p>
     * @param    projectId    id of project, an unique entity for identification
     * @return   all employee detrails who are in the project
     *           error statement if no one is in project
     */
    public String getAllEmployeesByProjectId(String projectId) {
        try {
            List<Employee> employees = projectService.
                                          getAllEmployeesByProjectId(projectId);
            String employeesDetails = "\n";
            if (null != employees) {
                ProjectView projectView = new ProjectView();
                projectView.showErrorMessage(ConstantString.ASSIGNED_EMPLOYEES 
                                                                   + projectId);
                for (Employee employee : employees) {
                    employeesDetails = employeesDetails + employee.toString();
                }
                return employeesDetails;
            } else {
                return ConstantString.INVALID_PROJECT_ID;
            }
        } catch (MyException myException) {
            ProjectView projectView = new ProjectView();
            projectView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        }  
        return null;
    }
}
        
               
        
        
    
