package com.ideas2it.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.ideas2it.common.ConstantString;
import com.ideas2it.exception.MyException;
import com.ideas2it.model.Address;
import com.ideas2it.model.Employee;
import com.ideas2it.service.AddressService;
import com.ideas2it.service.impl.AddressServiceImpl;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.view.EmployeeView;

/**
 * <p>
 * Controls the flow between the view and the service class
 * Provides the methods create, delete, update, fetching the employee details
 * </p> 
 * author Jegatheesh palanisamy
 * 27/07/2017
 */
public class EmployeeController {
    private EmployeeService employeeService = new EmployeeServiceImpl();
    
    /**
     * <p>
     * Adds new employee detail to employee table
     * </p>
     * @param    id          id of employee, an unique entity for identification 
     * @param    name        employee name, not necessarily unique
     * @param    designation employee designation, not necessarily unique
     * @param    mail        employee mail an unique entity for each employee
     * @param    dob         employee dob to be added
     */
    public void createEmployee(String id, String name, String designation, 
                                   String mail, String dob) {
        try {
            employeeService.insertEmployee(id, name, designation, mail,
                                                                       dob);
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        }
    }
    
    /**
     * <p>
     * adds the address details of the client
     * </p>
     * @param  id  id of employee, an unique entity for identification 
     * @param  addressType   unique for each client, defines the address' type
     * @param  doorNo    an address detail, does not need to be unique
     * @param  landmark  an address detail, does not need to be unique
     * @param  street    an address detail, does not need to be unique
     * @param  city      an address detail, does not need to be unique
     * @param  district  an address detail, does not need to be unique
     * @param  state     an address detail, does not need to be unique
     * @param  country   an address detail, does not need to be unique
     * @param  postcode  an address detail, does not need to be unique
     * @return  true if address is added else false
     */
    public void addAddress(String id, String addressType, String doorNo,
                                        String landMark, String street,
                                        String city, String district,
                                        String state, String country, 
                                        String postCode) throws MyException {
        AddressService addressService = new AddressServiceImpl();
        addressService.addAddress(id, addressType, doorNo, landMark,   
                                             street, city, district, state,
                                                         country, postCode);
    }
    
    /**
     * <p>
     * Removes address of employee by id
     * </p>
     * @param id  id of employee, an unique entity for identification 
     * @return  true if address is removed else false
     */
    public void removeAddressById(String id) throws MyException {
        AddressService addressService = new AddressServiceImpl();
        addressService.removeAddressById(id);
    }
    
    /**
     * <p>
     * adds the address details of the employee
     * </p>
     * @param  id  id of employee, an unique entity for identification 
     * @param  addressType   unique for each client, defines the address' type
     * @param  doorNo    an address detail, does not need to be unique
     * @param  landmark  an address detail, does not need to be unique
     * @param  street    an address detail, does not need to be unique
     * @param  city      an address detail, does not need to be unique
     * @param  district  an address detail, does not need to be unique
     * @param  state     an address detail, does not need to be unique
     * @param  country   an address detail, does not need to be unique
     * @param  postcode  an address detail, does not need to be unique
     * @return  true if address is updated else false
     */
    public boolean updateAddressById(String id, String addressType, 
                             String doorNo, String landMark, String street,
                              String city, String district, String state,
                           String country, String postCode) throws MyException {
        AddressService addressService = new AddressServiceImpl();
        try {
            return addressService.updateAddressById(id,addressType, doorNo,   
                                               landMark, street, city, district,
                                                state, country, postCode);
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        }
        return true;  
    }
    
    /**
     * <p>
     * Deletes particular employee detail by employee id 
     * </p>
     * @param   id   id of employee, an unique entity for identification 
     */ 
    public boolean removeEmployeeById(String id) throws MyException {
        try {
            return employeeService.removeEmployeeById(id);
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        } 
        return false;
    }
    
    /**
     * <p>
     * Gets the details of particular employee detail by employee id 
     * </p>
     * @param     id     id of employee, an unique entity for identification 
     * @return    employee details of the employee id or error message 
     *            if detail is null
     */
    public Employee getEmployeeById(String id) throws MyException {
        try {                                                  
            return employeeService.getEmployeeById(id);
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        }
        return null;        
    } 
    
    /**
     * @see com.ideas2it.service.EmployeeService;
     *                                     #getAllEmployees()
     */
    public List<Employee> getAllEmployees() throws MyException {                                                  
        try {
            return employeeService.getAllEmployees();
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);   
        }
        return null;
    }
    
    /**
     * <p>
     * Gets all employeedetail and returns 
     * </p>
     * @return   returns all the employees details or error message
     *           if details are null 
     */
    public List<Address> getAllAddresses() throws MyException {   
        AddressService addressService = new AddressServiceImpl();                                               
        try {
            return addressService.getAllAddresses();
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);         
        }
        return null;
    }
    
    /**
     * <p>
     * Gets all employee' address detail and returns 
     * </p>
     * @return   returns all the employees' address details or error message
     *           if details are null 
     */
    public List<Address> getAddressById(String id) throws MyException {
        AddressService addressService = new AddressServiceImpl();
        try {
            return addressService.getAddressById(id);
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);         
        }
        return null;
    } 
    
    /**
     * <p>
     * Replaces an employees old name by new one
     * </p>
     * @param    id        id of employee, an unique entity for identification 
     * @param    newName     holds the employee's new name
     */
    public boolean updateEmployeeName(String id , String name) 
                                                           throws MyException {
        try {
            return employeeService.updateEmployeeName(id, name);
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        } 
        return false;
    }
    
    /**
     * <p>
     * Replaces an employees old age by new one
     * </p>
     * @param      id     id of employee, an unique entity for identification 
     * @param    newDob   holds the employee's  newDob
     */
    public boolean updateEmployeeDob(String id, String dob) throws MyException {
        try {
            return employeeService.updateEmployeeDob(id, dob);
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        } 
        return false;
    }
    
    /**
     * <p>
     * Replaces an employees old designation by new one
     * </p>
     * @param    id                 id of employee, an unique entity for 
     *                              identification 
     * @param    newDesignation     holds the employee's new designation
     */
    public boolean updateEmployeeDesignation(String id, String designation) 
                                                       throws MyException {
        try {
            return employeeService.updateEmployeeDesignation(id, designation);
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(ConstantString.ERROR_MESSAGE_FOR_USER);
        } 
        return false;
    }
    
    /**
     * <p>
     * Replaces an employees old mail by new one
     * </p>
     * @param    id        id of employee, an unique entity for identification 
     * @param    newMail   holds the employee's new mail
     */  
    public boolean updateEmployeeMail(String id, String mail) 
                                                           throws MyException {
        try {
            return employeeService.updateEmployeeMail(id, mail);
        } catch (MyException myException) {
            EmployeeView employeeView = new EmployeeView();
            employeeView.showErrorMessage(
                                        ConstantString.ERROR_MESSAGE_FOR_USER);
        }      
        return false;      
    }
    
    /**
     * @see com.ideas2it.service.EmployeeService;
     *                         findValidEmployeeDetails(String id, String name, 
     *                             String designation, String mail, String dob
     */
    public int[] findValidEmployeeDetails(String id, String name, 
                                  String designation, String mail, String dob) 
                                                           throws MyException {
        return employeeService.findValidEmployeeDetails(id, name, designation,
                                                                    mail, dob);
    }   
}
